#ifndef RCDVMEINTERRUPT_H
#define RCDVMEINTERRUPT_H

//******************************************************************************
// file: RCDVmeInterrupt.h
// desc: VMEbus C++ wrapper library, VMEbus interrupt class
// auth: 23/10/01 R. Spiwoks
// modf: 02/12/02 R. Spiwoks, DataFlow repository
//******************************************************************************

// $Id$

#include "RCDVme/RCDVmeTypes.h"

namespace RCD {

class VME;

//------------------------------------------------------------------------------

class VMEInterrupt {

  public:

    // main members
    u_int Wait(int time_out, VMEInterruptInfo& interrupt_info);
    u_int RegisterSignal(int signal_number);
    u_int InfoGet(VMEInterruptInfo& interrupt_info);
    u_int Reenable();

    // helper
    u_int Dump() const;

    // operator to return status of object
    u_int operator()()  { return(my_status); };

    // friend
    friend class VME;

  private:
    VMEInterrupt(const VMEInterruptList& interrupt_list);
   ~VMEInterrupt();

    VMEInterruptList    my_interrupt_list;
    int                 my_identifier;
    u_int               my_status;
};

}       // namespace RCD

#endif  // RCDVMEINTERRUPT_H
