#ifndef RCDVMEMASTER_MAP_H
#define RCDVMEMASTER_MAP_H

//******************************************************************************
// file: RCDVmeMasterMap.h
// desc: VMEbus C++ wrapper library, VME master mapping class
// auth: 17/09/01 R. Spiwoks
// modf: 23/10/01 R. Spiwoks, VMEbus API draft
// modf: 02/12/02 R. Spiwoks, DataFlow repository
//******************************************************************************

// $Id$

#include "RCDVme/RCDVmeTypes.h"

namespace RCD {

class VME;

//------------------------------------------------------------------------------

class VMEMasterMap {

  public:

    // safe read and write
    u_int ReadSafe(u_int address_offset, u_int* value);
    u_int WriteSafe(u_int address_offset, u_int data);

    u_int ReadSafe(u_int address_offset, u_short* value);
    u_int WriteSafe(u_int address_offset, u_short data);

    u_int ReadSafe(u_int address_offset, u_char* value);
    u_int WriteSafe(u_int address_offset, u_char data);

    // fast read and write
    inline void ReadFast(u_int address_offset, u_int* value) { *value = *(volatile u_int*)(my_virtual_address + address_offset); }
    inline void WriteFast(u_int address_offset, u_int data) { *(volatile u_int*)(my_virtual_address + address_offset) = data; }

    inline void ReadFast(u_int address_offset, u_short* value) { *value = *(volatile u_short*)(my_virtual_address + address_offset); }
    inline void WriteFast(u_int address_offset, u_short data) { *(volatile u_short*)(my_virtual_address + address_offset) = data; }

    inline void ReadFast(u_int address_offset, u_char* value) { *value = *(volatile u_char*)(my_virtual_address + address_offset); }
    inline void WriteFast(u_int address_offset, u_char data) { *(volatile u_char*)(my_virtual_address + address_offset) = data; }

    // helpers
    inline u_long VirtualAddress(u_long* virtual_address) const { *virtual_address = my_virtual_address; return(my_status); }
    inline u_int VmebusAddress(u_int* vmebus_address) const { *vmebus_address = my_master_map.vmebus_address; return(my_status); }
    u_int Dump() const;

    // operator to return status of object
    u_int operator()()  { return(my_status); };

    // friend
    friend class VME;

  private:
    VMEMasterMap(u_int VMEbus_address, u_int window_size, u_int address_modifier, u_int options);
   ~VMEMasterMap();

    int                 my_identifier;
    VME_MasterMap_t     my_master_map;
    u_long              my_virtual_address;
    u_int               my_status;
};

}       // namespace RCD

#endif  // RCDVMEMASTER_MAP_H
