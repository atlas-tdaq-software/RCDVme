#ifndef RCDVMETYPES_H
#define RCDVMETYPES_H

//******************************************************************************
// file: RCDVmeTypes.h
// desc: VMEbus C++ wrapper library, VME type definitions
// auth: 17/09/01 R. Spiwoks
// modf: 23/10/01 R. Spiwoks, VMEbus API draft
// modf: 02/12/02 R. Spiwoks, DataFlow repository
// modf: 19/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
//******************************************************************************

// $Id$

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

//#include "DFDebug/DFDebug.h"

namespace RCD {

// bus error information
typedef VME_BusErrorInfo_t VMEBusErrorInfo;

// VMEbus interrupt information
typedef VME_InterruptItem_t VMEInterruptItem;
typedef VME_InterruptList_t VMEInterruptList;
typedef VME_InterruptInfo_t VMEInterruptInfo;

// block transfer
typedef VME_BlockTransferItem_t VMEBlockTransferItem;
typedef VME_BlockTransferList_t VMEBlockTransferList;

}       // namespace RCD

#endif  // RCDVMETYPES_H
