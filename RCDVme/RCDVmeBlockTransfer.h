#ifndef RCDVMEBLOCKTRANSFER_H
#define RCDVMEBLOCKTRANSFER_H

//******************************************************************************
// file: RCDVmeBlockTransfer.h
// desc: VMEbus C++ wrapper library, VME block transfer class
// auth: 19/09/01 R. Spiwoks
// modf: 23/10/01 R. Spiwoks, VMEbus API draft
// modf: 02/12/02 R. Spiwoks, DataFlow repository
//******************************************************************************

// $Id$

#include "RCDVme/RCDVmeTypes.h"

namespace RCD {

class VME;

//------------------------------------------------------------------------------

class VMEBlockTransfer {

  public:

    // main members
    u_int Start();
    u_int Wait(int time_out);

    // helpers
    u_int Status(int position_of_block, u_int* status) const;
    u_int Remaining(int position_of_block, u_int* remaining) const;
    u_int Dump() const;

    // operator to return status of object
    u_int operator()()          { return(my_status); };

    // friend
    friend class VME;

  private:
    VMEBlockTransfer(const VMEBlockTransferList& block_transfer_list);
   ~VMEBlockTransfer();

    VMEBlockTransferList        my_block_transfer_list;
    int                         my_identifier;
    u_int                       my_status;
};

}       // namespace RCD

#endif  // RCDVMEBLOCKTRANSFER_H
