#ifndef RCDCMEMSEGMENT_H
#define RCDCMEMSEGMENT_H

//******************************************************************************
// file: RCDCmemSegment.hh
// desc: C++ wrapper for RCD CMEM driver/library
// auth: 06/06/02 R. Spiwoks
// modf: 02/12/02 R. Spiwoks, DataFlow repository
//******************************************************************************

// $Id$

#include <string>

#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"

#include "RCDVme/RCDVmeTypes.h"

namespace RCD {

//------------------------------------------------------------------------------

class CMEMSegment {

  public:
    CMEMSegment(std::string, u_int, bool=false);
   ~CMEMSegment();

    // helpers
    static u_int Dump();
    u_int Size() const                  { return(my_size); }
    bool  BPA() const                   { return(my_bpa); }
    u_long VirtualAddress() const       { return(my_virtual_address); }
    u_long PhysicalAddress() const      { return(my_physical_address); }
    int operator()() const              { return(my_status); }

    // public methods
    int dump(int) const;
    int fill(int, u_int, u_int);
    int writeToFile(const std::string&, int);
    int readFromFile(const std::string&, int);

  private:
    static int          my_user;

    std::string         my_name;
    int                 my_size;
    bool                my_bpa;
    int                 my_identifier;
    u_long              my_virtual_address;
    u_long              my_physical_address;
    u_int               my_status;
};

}       // namespace RCD

#endif  // RCDCMEMSEGMENT_H
