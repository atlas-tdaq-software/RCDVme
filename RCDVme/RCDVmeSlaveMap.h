#ifndef RCDVMESLAVEMAP_H
#define RCDVMESLAVEMAP_H

//******************************************************************************
// file: RCDVmeSlaveMap.h
// desc: VMEbus C++ wrapper library, VME slave mapping class
// auth: 19/09/01 R. Spiwoks
// modf: 23/10/01 R. Spiwoks, VMEbus API draft
// modf: 02/12/02 R. Spiwoks, DataFlow repository
//******************************************************************************

// $Id$

#include "RCDVme/RCDVmeTypes.h"

namespace RCD {

class VME;

//------------------------------------------------------------------------------

class VMESlaveMap {

  public:

    // helpers
    inline u_int VmebusAddress(u_int* vmebus_address) const { *vmebus_address = my_vmebus_address; return(my_status); }
    u_int Dump() const;

    // operator to return status of object
    u_int operator()()  { return(my_status); };

    // friend
    friend class VME;

  private:
    VMESlaveMap(u_int system_iobus_address, u_int window_size, u_int address_width, u_int options);
   ~VMESlaveMap();

    int                 my_identifier;
    VME_SlaveMap_t      my_slave_map;
    u_int               my_vmebus_address;
    u_int               my_status;

};

}       // namespace RCD

#endif  // RCDVMESLAVEMAP_H
