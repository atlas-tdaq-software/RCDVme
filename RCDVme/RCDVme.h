#ifndef RCDVME_H
#define RCDVME_H

//******************************************************************************
// file: RCDVme.h
// desc: VMEbus C++ wrapper library, main VME class and main include file
// auth: 17/09/01 R. Spiwoks
// modf: 23/10/01 R. Spiwoks, VMEbus API draft
// modf: 02/12/02 R. Spiwoks, DataFlow repository
//******************************************************************************

// $Id$

#include <string>

#include "RCDVme/RCDVmeTypes.h"
#include "RCDVme/RCDVmeBlockTransfer.h"
#include "RCDVme/RCDVmeInterrupt.h"
#include "RCDVme/RCDVmeMasterMap.h"
#include "RCDVme/RCDVmeSlaveMap.h"
#include "RCDVme/RCDCmemSegment.h"

namespace RCD {

// VME main class (contains all global resources) ------------------------------

class VME {

  public:

    // singleton methods
    static VME* Open();
    static u_int  Close();

    // members for return codes
    int ErrorPrint(u_int error_code);
    int ErrorString(u_int error_code, std::string* error_string);
    int ErrorNumber(u_int error_code, int* error_number);

    // members for CR/CSR access
    u_int ReadCRCSR(int slot, u_int crcsr_field, u_int* value);
    u_int WriteCRCSR(int slot, u_int crcsr_field, u_int data);

    // member for bus error handling
    u_int BusErrorRegisterSignal(int signal_number);
    u_int BusErrorInfoGet(VMEBusErrorInfo& bus_error_info);

    // factory methods
    VMEMasterMap* MasterMap(u_int vmebus_address, u_int window_size, u_int address_modfier, u_int options);
    u_int MasterUnmap(VMEMasterMap* master_map);

    VMESlaveMap* SlaveMap(u_int system_iobus_address, u_int window_size, u_int address_width, u_int options);
    u_int SlaveUnmap(VMESlaveMap* slave_map);

    VMEBlockTransfer* BlockTransfer(const VMEBlockTransferList& block_transfer_list);
    u_int BlockTransferDelete(VMEBlockTransfer* block_transfer);
    u_int BlockTransfer(const CMEMSegment&,u_int,int,int);
    u_int BlockTransfer(u_int,u_int,int,int);
    u_int RunBlockTransfer(const CMEMSegment&, const u_int, const u_int, const u_int, const u_int, const u_int msiz = 0xffffffff, const bool incr = false);

    VMEInterrupt* Interrupt(const VMEInterruptList& interrupt_list);
    u_int InterruptDelete(VMEInterrupt* interrupt);

    u_int InterruptGenerate(int level, u_char vector);

    // VME driver/library status dumps
    u_int MasterMapDump() const         { return(VME_MasterMapDump()); }
    u_int SlaveMapDump() const          { return(VME_SlaveMapDump()); }
    u_int BlockTransferDump() const     { return(VME_BlockTransferDump()); }
    u_int InterruptDump() const         { return(VME_InterruptDump()); }

    // operator to return status of object
     u_int operator()()                 { return(my_status); };

    // friends
    friend class VMEMasterMap;
    friend class VMESlaveMap;
    friend class VMEBlockTransfer;
    friend class VMEInterrupt;

  private:
    static const int            TIMEOUT;
    static const int            WAITTIME;
    static const int            POLLTIME;

    static VME*                 my_instance;
    static int                  my_users;
    static u_int                my_status;

    VME();
   ~VME();
};

}       // namespace RCD

#endif  // RCDVME_H
