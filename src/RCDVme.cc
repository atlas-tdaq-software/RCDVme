//******************************************************************************
// file: RCDVme.cc
// desc: VMEbus C++ wrapper library, main VME class
// auth: 17/09/01 R. Spiwoks
// modf: 23/10/01 R. Spiwoks, VMEbus API draft
// modf: 02/12/02 R. Spiwoks, DataFlow repository
// modf: 19/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
//******************************************************************************

// $Id$

#include "RCDVme/RCDVme.h"
#include "DFDebug/DFDebug.h"
#include "RCDUtilities/RCDUtilities.h"
#include <errno.h>

using namespace RCD;
using namespace std;

//------------------------------------------------------------------------------

const int       VME::TIMEOUT            = 10000;        //  10 seconds !!!!
const int       VME::WAITTIME           = 10000;        //  10 seconds !!!!
const int       VME::POLLTIME           = 10000000;     // ~10 seconds !!!!

VME*            VME::my_instance        = 0;
int             VME::my_users           = 0;
u_int           VME::my_status          = VME_SUCCESS;

//------------------------------------------------------------------------------

VME* VME::Open() {

    if(my_users == 0) {
        my_instance = new VME();
    }
    my_users++;
    return(my_instance);
}

//------------------------------------------------------------------------------

u_int VME::Close() {

    my_users--;
    if(my_users == 0) {
        delete my_instance;
    }
    return(VME_SUCCESS);
}

//------------------------------------------------------------------------------

int VME::ErrorPrint(u_int error_code) {

    return(VME_ErrorPrint(error_code));
}

//------------------------------------------------------------------------------

int VME::ErrorString(u_int error_code, string* error_string) {

    char error_char[VME_MAXSTRING];
    int        return_code;

    return_code = VME_ErrorString(error_code, error_char);
    error_string->assign(error_char);

    return(return_code);
}

//------------------------------------------------------------------------------

int VME::ErrorNumber(u_int error_code, int* error_number) {

    return(VME_ErrorNumber(error_code, error_number));
}

//------------------------------------------------------------------------------

u_int VME::ReadCRCSR(int slot, u_int crcsr_field, u_int* value) {

    return(VME_ReadCRCSR(slot, crcsr_field, value));
}

//------------------------------------------------------------------------------

u_int VME::WriteCRCSR(int slot, u_int crcsr_field, u_int data) {

    return(VME_WriteCRCSR(slot, crcsr_field, data));
}

//------------------------------------------------------------------------------

u_int VME::BusErrorRegisterSignal(int signal_number) {

    return(VME_BusErrorRegisterSignal(signal_number));
}

//------------------------------------------------------------------------------

u_int VME::BusErrorInfoGet(VMEBusErrorInfo& bus_error_info) {

    return(VME_BusErrorInfoGet(&bus_error_info));
}

//------------------------------------------------------------------------------

VMEMasterMap* VME::MasterMap(u_int vmebus_address, u_int window_size, u_int address_modifier, u_int options) {

    return(new VMEMasterMap(vmebus_address, window_size, address_modifier, options));
}

//------------------------------------------------------------------------------

u_int VME::MasterUnmap(VMEMasterMap* master_map) {

    delete master_map;

    return(VME_SUCCESS);
}

//------------------------------------------------------------------------------

VMESlaveMap* VME::SlaveMap(u_int system_iobus_address, u_int window_size, u_int address_modifier, u_int options) {

    return(new VMESlaveMap(system_iobus_address, window_size, address_modifier, options));
}

//------------------------------------------------------------------------------

u_int VME::SlaveUnmap(VMESlaveMap* slave_map) {

    delete slave_map;

    return(VME_SUCCESS);
}

//------------------------------------------------------------------------------

VMEBlockTransfer* VME::BlockTransfer(const VMEBlockTransferList& block_transfer_list) {

    return(new VMEBlockTransfer(block_transfer_list));
}

//------------------------------------------------------------------------------

u_int VME::BlockTransferDelete(VMEBlockTransfer* block_transfer) {

    delete block_transfer;

    return(VME_SUCCESS);
}

//------------------------------------------------------------------------------

u_int VME::BlockTransfer(const CMEMSegment& seg, u_int vme_addr, int blk_size, int dir) {

    // check size of CMEM segment
    if((blk_size < 2) || (blk_size > static_cast<int>(seg.Size()))) {
        CERR("wrong size \"%d\", expected [2..%d]",blk_size,seg.Size());
        return(EINVAL);
    }

    RCD::VMEBlockTransferList list;
    RCD::VMEBlockTransfer* block;
    u_int stat;

    // fill block transfer item parameters
    list.list_of_items[0].vmebus_address       = vme_addr;
    list.list_of_items[0].system_iobus_address = seg.PhysicalAddress();
    list.list_of_items[0].size_requested       = blk_size * sizeof(u_int);
    list.list_of_items[0].control_word         = (dir) ? VME_DMA_D32W : VME_DMA_D32R;
    list.number_of_items = 1;

    CDBG("vme = 0x%08x, pci = 0x%08x, size = 0x%08x, ctrl = 0x%08x",list.list_of_items[0].vmebus_address,list.list_of_items[0].system_iobus_address,list.list_of_items[0].size_requested,list.list_of_items[0].control_word);

    // create VME block transfer
    block = BlockTransfer(list);
    if((*block)()) {
        CERR("creating VME block transfer","");
        ErrorPrint((*block)());
        return((*block)());
    }

    // start VME block transfer
    if((stat = block->Start()) != VME_SUCCESS) {
        CERR("starting VME block transfer","");
        ErrorPrint(stat);
        BlockTransferDelete(block);
        return(stat);
    }

    // wait for VME block transfer
    if((stat = block->Wait(TIMEOUT)) != VME_SUCCESS) {
        CERR("waiting for VME block transfer","");
        ErrorPrint(stat);

        // status of block
        block->Status(0,&stat);
        CERR("status of block 0","");
        ErrorPrint(stat);

        // remaining of block
        block->Remaining(0,&stat);
        CERR("remaining of block 0 = %d",stat);
    }

    // delete VME block transfer
    BlockTransferDelete(block);

    return(stat);
}

//------------------------------------------------------------------------------

u_int VME::BlockTransfer(u_int pci_addr, u_int vme_addr, int blk_size, int dir) {

    RCD::VMEBlockTransferList list;
    RCD::VMEBlockTransfer* block;
    u_int stat;

    // fill block transfer item parameters
    list.list_of_items[0].vmebus_address       = vme_addr;
    list.list_of_items[0].system_iobus_address = pci_addr;
    list.list_of_items[0].size_requested       = blk_size * sizeof(u_int);
    list.list_of_items[0].control_word         = (dir) ? VME_DMA_D32W : VME_DMA_D32R;
    list.number_of_items = 1;

    CDBG("vme = 0x%08x, pci = 0x%08x, size = 0x%08x, ctrl = 0x%08x",list.list_of_items[0].vmebus_address,list.list_of_items[0].system_iobus_address,list.list_of_items[0].size_requested,list.list_of_items[0].control_word);

    // create VME block transfer
    block = BlockTransfer(list);
    if((*block)()) {
        CERR("creating VME block transfer","");
        ErrorPrint((*block)());
        return((*block)());
    }

    // start VME block transfer
    if((stat = block->Start()) != VME_SUCCESS) {
        CERR("starting VME block transfer","");
        ErrorPrint(stat);
        return(stat);
    }

    // wait for VME block transfer
    if((stat = block->Wait(TIMEOUT)) != VME_SUCCESS) {
        CERR("waiting for VME block transfer","");
        ErrorPrint(stat);

        // status of block
        block->Status(0,&stat);
        CERR("status of block 0","");
        ErrorPrint(stat);

        // remaining of block
        block->Remaining(0,&stat);
        CERR("remaining of block 0 = %d",stat);
    }

    // delete VME block transfer
    BlockTransferDelete(block);

    return(stat);
}

//------------------------------------------------------------------------------

u_int VME::RunBlockTransfer(const CMEMSegment& seg, const u_int off, const u_int addr, const u_int size, const u_int dir, const u_int msiz, const bool incr) {

    // check size of CMEM segment
    if(seg.Size() < (size + off)*sizeof(u_int)) {
        CERR("segment too small, CMEM = 0x%08x, size = 0x%08x",seg.Size(),(size + off)*sizeof(u_int));
        return(EINVAL);
    }

    RCD::VMEBlockTransferList list;
    RCD::VMEBlockTransfer* block;
    u_int rsiz, isiz, sadd, stat;
    long unsigned int soff;
    int item;

    // fill block transfer item parameters
    rsiz = size;
    sadd = addr;
    soff = seg.PhysicalAddress() + off*sizeof(u_int);
    item = 0;
    while(rsiz) {
        isiz = (rsiz >= msiz) ? msiz : rsiz;
        list.list_of_items[item].vmebus_address         = sadd;
        list.list_of_items[item].system_iobus_address   = soff;
        list.list_of_items[item].size_requested         = isiz*sizeof(u_int);
        list.list_of_items[item].control_word           = dir;
        list.number_of_items = item + 1;
        rsiz -= isiz;
        if(incr) sadd += isiz*sizeof(u_int);
        soff += isiz*sizeof(u_int);
        if((++item) > VME_MAXCHAINEL) {
            CERR("Number of DMA chain elements %d exceeds maximum allowed %d",item,VME_MAXCHAINEL);
            return(VME_NOCHAINMEM);
        }
    }

#ifdef DEBUG
    for(item=0; item<list.number_of_items; item++) {
        CDBG("DMA item %2d: vme = 0x%08x, pci = 0x%016lx, size = 0x%08x, ctrl = 0x%08x",item,list.list_of_items[item].vmebus_address,list.list_of_items[item].system_iobus_address,list.list_of_items[item].size_requested,list.list_of_items[item].control_word);
    }
#endif  // DEBUG

    // create VME block transfer
    block = BlockTransfer(list);
    if((*block)()) {
        CERR("creating VME block transfer:","");
        ErrorPrint((*block)());
        return((*block)());
    }

    // start VME block transfer
    int itim = 0;
    while(true) {
        if((stat = block->Start()) == VME_SUCCESS) break;
        if(stat != VME_DMABUSY) {
            CERR("starting VME block transfer:","");
            ErrorPrint(stat);
            BlockTransferDelete(block);
            return(stat);
        }
        if(++itim >= POLLTIME) {
            CERR("timeout starting VME block transfer","");
            BlockTransferDelete(block);
            return(VME_TIMEOUT);
        }
        sched_yield();
    }

    // wait for VME block transfer
    if((stat = block->Wait(WAITTIME)) != VME_SUCCESS) {
        CERR("waiting for VME block transfer:","");
        ErrorPrint(stat);
        block->Dump();
    }

    // delete VME block transfer
    BlockTransferDelete(block);

    return(stat);
}

//------------------------------------------------------------------------------

VMEInterrupt* VME::Interrupt(const VMEInterruptList& interrupt_list) {

    return(new VMEInterrupt(interrupt_list));
}

//------------------------------------------------------------------------------

u_int VME::InterruptDelete(VMEInterrupt* interrupt) {

    delete interrupt;

    return(VME_SUCCESS);
}

//------------------------------------------------------------------------------

u_int VME::InterruptGenerate(int level, u_char vector) {

    return(VME_InterruptGenerate(level, vector));
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

VME::VME() {

    // open VME library
    if((my_status = VME_Open()) != VME_SUCCESS) {
        VME_ErrorPrint(my_status);
        ERR_TEXT(DFDB_RCDVME,"opening VME driver/library");
        return;
    }
    DEBUG_TEXT(DFDB_RCDVME,15,"RCD VMEbus driver/library opened");
}

//------------------------------------------------------------------------------

VME::~VME() {

    // close VME library
    VME_Close();
    DEBUG_TEXT(DFDB_RCDVME,15,"RCD VMEbus driver/library closed");
}
