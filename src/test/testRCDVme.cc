//******************************************************************************
// file: testRCDVme.cc
// desc: program to read from and write to VME address
// auth: 27/04/99 I. Brawn, original version
// modf: 26/03/00 R. Spiwoks, C++ and generalized program structure
// modf: 11/05/01 R. Spiwoks, Linux and DAQ/EF -1 VME libs
// modf: 02/12/02 R. Spiwoks, DataFlow repository
// modf: 19/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
// modf: 13/07/04 R. Spiwoks: corrected CR/CSR, added write block
// modf: 14/07/04 R. Spiwoks: modified input of arguments
// modf: 22-JAN-2014 R. Spiwoks: capture arrows, command history, RCDUtilities
//******************************************************************************

// $Id$

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <signal.h>
#include <errno.h>
#include <getopt.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <sys/time.h>
#ifdef TERMIOS
#include <termios.h>
#include <unistd.h>
#endif  // TERMIOS
#include <readline/readline.h>
#include <readline/history.h>

#include "RCDUtilities/RCDUtilities.h"
#include "RCDMenu/RCDMenu.h"
#include "RCDVme/RCDVme.h"
#include "RCDVme/RCDCmemSegment.h"

using namespace RCD;

CMEMSegment*    seg;
VME*            vme;
VMEMasterMap*   vmm;

u_int           g_base = 0x00000000;
u_int           g_size = 0x00000000;
u_int           g_awid = 0x00000000;
u_int           g_bsiz = 0x00010000;
bool            g_bbpa = false;
u_int           g_debg = 0;
u_int           g_prnt = 10000;
u_int           g_hist = 100;

jmp_buf         jump_buff;
int             jump_flag = 1;
int             g_sig;

static const unsigned int       ADDR_CRCSR_MAX  = 0x0007ffff;
static const unsigned int       DWID_CRCSR_MASK = 0x0f000000;

//------------------------------------------------------------------------------

class TVME_BUFFER {

  public:

    static const int            SUCCESS         = 0;
    static const int            ERROR_NODATA    = -1;
    static const int            ERROR_WRONGPAR  = -2;

    TVME_BUFFER(const unsigned int);
   ~TVME_BUFFER();

    unsigned int size()         { return(m_size); }

    int putNext(const std::string&);
    int getPrev(const unsigned int, std::string&);
    unsigned int fill()         { return(m_fc); }
    void reset();

  private:
    unsigned int                m_size;
    std::vector<std::string>    m_data;
    unsigned int                m_wp;
    unsigned int                m_rp;
    unsigned int                m_fc;
};

//------------------------------------------------------------------------------

TVME_BUFFER::TVME_BUFFER(const unsigned int s) : m_size(s) {

    reset();
}

//------------------------------------------------------------------------------

TVME_BUFFER::~TVME_BUFFER() {

    // nothing tio be done
}

//------------------------------------------------------------------------------

void TVME_BUFFER::reset() {

    m_data.clear();
    m_data.resize(m_size);
    m_wp = 0;
    m_rp = 0;
    m_fc = 0;
}

//------------------------------------------------------------------------------

int TVME_BUFFER::putNext(const std::string& str) {

    m_data[m_wp] = str;
    m_wp++; if(m_wp == m_size) m_wp = 0;
    m_fc++;
    if(m_fc > m_size) {
        m_rp++; if(m_rp == m_size) m_rp = 0;
        m_fc = m_size;
    }
    CDBG("wp = %d, rp = %d, fc = %d\n",m_wp,m_rp,m_fc);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int TVME_BUFFER::getPrev(const unsigned int idx, std::string& str) {

    if((idx == 0) || (idx > m_fc)) return(ERROR_WRONGPAR);

    if(m_fc == 0) return(ERROR_NODATA);

    unsigned int ip;

    ip = m_rp + m_fc - idx; if(ip >= m_size) ip -= m_size;
    CDBG("index = %d, wp = %d, rp = %d, fc = %d, ip = %d\n",idx,m_wp,m_rp,m_fc,ip);
    str = m_data[ip];

    return(SUCCESS);
}

//------------------------------------------------------------------------------

void tvme_usage();
int tvme_block(const u_int, const u_int, const u_int);
int tvme_bdump(const u_int);
int tvme_bfill(const u_int, const u_int, const u_int = 0);
int tvme_rfile(const std::string&, const unsigned int);
int tvme_wfile(const std::string&, const unsigned int);
double tvme_gtime();
std::string tvme_gfreq(const double);
std::string tvme_grate(const double);
int tvme_split(const std::string&, std::vector<std::string>&);
#ifdef TERMIOS
int tvme_input(string&);
void tvme_canon(bool);
#endif  // TERMIOS

//------------------------------------------------------------------------------

void catchInterrupt(int sig) {

    CDBG("return from interrupt with signal %d",sig);
    g_sig = sig;

    // jump to main input polling
    jump_flag++;
    siglongjmp(jump_buff,1);
}

//------------------------------------------------------------------------------

class TVMEOpen : public RCD::MenuItem {
  public:
    TVMEOpen() { setName("open TVME"); }
    int action() {

        // get base address
        if(g_base == 0x00000000) {
            g_base = (u_int)enterHex("Vmebus_address                (hex)",0,0xffffffff);
        }
        if(g_base < 0x100) g_base = g_base << 24;

        // get window size
        if(g_size == 0x00000000) {
            g_size = (u_int)enterHex("Window_size                   (hex)",0,0xffffffff);
        }

        // get address width
        if(g_awid == 0x00000000) {
            g_awid = (u_int)enterHex("Address_width (0=A16, 1=A24, 2=A32)",0,2);
        }
        if(g_awid == 0) g_base &= 0x0000ffff;
        if(g_awid == 1) g_base &= 0x00ffffff;

        // open VME library
        vme = VME::Open();
        if((*vme)()) {
            std::cerr << ">> ERROR opening VME object!!!!" << std::endl;
            vme->ErrorPrint((*vme)());
            std::exit(ENXIO);
        }

        // create VME master mapping
        vmm = vme->MasterMap(g_base,g_size,g_awid,0);
        if((*vmm)()) {
            std::cerr << ">> ERROR opening VME master mapping!!!!" << std::endl;
            vme->ErrorPrint((*vmm)());
            std::exit(ENXIO);
        }

        // create new contiguous memory segment
        seg = new CMEMSegment("testRCDVme",g_bsiz,g_bbpa);
        if((*seg)()) {
            std::cerr << ">> ERROR opening CMEM segment!!!!" << std::endl;
            std::exit(ENXIO);
        }

        return(0);
    }
};

//------------------------------------------------------------------------------

void tvme_usage() {

    std::cout << "-------------------------------------------------------------------------------------" << std::endl;
    std::cout << "testRCDVme <OPTIONS>:" << std::endl;
    std::cout << "-------------------------------------------------------------------------------------" << std::endl;
    std::printf(" -b <hex>  => VME base address                      (def = 0x%08x)\n",g_base);
    std::printf(" -s <hex>  => VME window size                       (def = 0x%08x (= %d))\n",g_size,g_size);
    std::printf(" -a <hex>  => VME address width (0=A16,1=A24,2=A32) (def = \"%s\")\n",g_awid==0?"A16":(g_awid==1)?"A24":"A32");
    std::printf(" -c <hex>  => CMEM segment size                     (def = 0x%08x (= %d))\n",g_bsiz,g_bsiz);
    std::printf(" -B        => CMEM segment using BPA                (def = %s)\n",g_bbpa?"YES":"NO");
    std::printf(" -d <int>  => DEBUG print level                     (def = %d)\n",g_debg);
    std::printf(" -p <int>  => PRINT parameter for loop read/write   (def = %d)\n",g_prnt);
    std::printf(" -H <int>  => Command history depth                 (def = %d)\n",g_hist);
    std::printf(" -h        => Print this help\n");
}

//------------------------------------------------------------------------------

int main (int argc, char *argv[]) {

    std::string in, op;
    int i, c;

    u_int data, addr, dati, offs, incr;
    u_short dats;
    int loop, eflg, slot;

#ifndef TERMIOS
    char* input;
#endif  // TERMIOS
    std::vector<std::string> arg;

    // read command line arguments
    while((c = getopt(argc,argv,":a:b:Bc:d:H:p:s:h")) != -1) {
        switch(c) {
        case 'a':
            g_awid = (u_int)strtoul(optarg,(char **)0,16);
            break;

        case 'b':
            g_base = (u_int)strtoul(optarg,(char **)0,16);
            break;

        case 'B':
            g_bbpa = true;
            break;

        case 'c':
            g_bsiz = (u_int)strtoul(optarg,(char **)0,16);
            break;

        case 'd':
            g_debg = (u_int)strtoul(optarg,(char **)0,10);
            break;

        case 'H':
            g_hist = (u_int)strtoul(optarg,(char **)0,10);
            break;

        case 'p':
            g_prnt = (u_int)strtoul(optarg,(char **)0,10);
            break;

        case 's':
            g_size = (u_int)strtoul(optarg,(char **)0,16);
            break;

        case 'h':
            tvme_usage();
            std::exit(0);
            break;

        case '?':
            CERR("invalid option \"%c\"",optopt);
            tvme_usage();
            std::exit(-1);
            break;

        case ':':
            CERR("missing argument for option \"%c\"",optopt);
            tvme_usage();
            std::exit(-1);
            break;

        default:
            break;
        }
    }

    // create VMEbus master mapping
    TVMEOpen menu;
    menu.action();

    // install signal handler: catch SIGINT
    if(signal(SIGINT,catchInterrupt)) {
        std::cout << "ERROR installing signal handler" << std::endl;
        std::exit(ENXIO);
    }
    CDBG("signal handler for SIGINT installed","");
    std::cout << "----------------------------------------------------------------" << std::endl;

    // read commands from command line
    std::cout << "enter commands (h for help)" << std::endl;

    while(true) {

        // set return for interrupt handler
        while(jump_flag) {
            jump_flag--;
            if(sigsetjmp(jump_buff,1)) std::cout << std::endl;
        }

#ifdef TERMIOS
        std::cout << ">>> ";
        if(tvme_input(in) != 0) {
            CERR("reading from input","");
            return(-1);
        }
#else
        input = readline(">>> ");
        if(!input) continue;
        add_history(input);
        in = input;
#endif  // TERMIOS

        addr = 0x0;
        data = 0x0;
        loop = 1;
        incr = 0x0;

        if(tvme_split(in,arg) == 0) continue;
        if(g_debg>=1) for(u_int i=0; i<arg.size(); i++) std::printf("arg[%d] = \"%s\"\n",i,arg[i].c_str());

        op = arg[0];
        if(arg.size() > 1) {
            if(sscanf(arg[1].c_str(),"%x",&addr) != 1) {
                std::printf("ERROR scanning <addr> from command\n");
                continue;
            }
        }
        if(arg.size() > 2) {
            if(sscanf(arg[2].c_str(),"%x",&data) != 1) {
                std::printf("ERROR scanning <data> from command\n");
                continue;
            }
        }
        if(arg.size() > 3) {
            if(sscanf(arg[3].c_str(),"%d",&loop) != 1) {
                std::printf("ERROR scanning <loop> from command\n");
                continue;
            }
        }

        // read short ----------------------------------------------------------
        if(op == "r") {
            if(addr >= g_size) {
                std::printf("ERROR - illegal address \"0x%08x\"\n",addr);
                continue;
            }
            if(arg.size() > 4) {
                if(sscanf(arg[4].c_str(),"%x",&incr) != 1) {
                    std::printf("ERROR scanning <incr> from command\n");
                    continue;
                }
            }
            if(loop == -1) {
                while(true) {
                    dats = 0x0;
                    if(vmm->ReadSafe(addr,&dats) == 0) {
                        std::printf("read data 0x%04x from address 0x%06x\n",dats,addr);
                    }
                    else {
                        std::printf("bus ERROR reading from address 0x%06x\n",addr);
                    }
                    addr += incr;
                }
            }
            else {
                for(i=0; i<loop; i++) {
                    dats = 0x0;
                    if(vmm->ReadSafe(addr,&dats) == 0) {
                        std::printf("read data 0x%04x from address 0x%06x\n",dats,addr);
                    }
                    else {
                        std::printf("bus ERROR reading from address 0x%06x\n",addr);
                    }
                    addr += incr;
                }
            }
        }

        // read long -----------------------------------------------------------
        else if(op == "rl") {
            if(addr >= g_size) {
                std::printf("ERROR - illegal address \"0x%08x\"\n",addr);
                continue;
            }
            if(arg.size() > 4) {
                if(sscanf(arg[4].c_str(),"%x",&incr) != 1) {
                    std::printf("ERROR scanning <incr> from command\n");
                    continue;
                }
            }
            if(loop == -1) {
                while(true) {
                    dati = 0x0;
                    if(vmm->ReadSafe(addr,&dati) == 0) {
                        std::printf("read data 0x%08x from address 0x%06x\n",dati,addr);
                    }
                    else {
                        std::printf("bus ERROR reading from address 0x%06x\n",addr);
                    }
                    addr += incr;
                }
            }
            else {
                for(i=0; i<loop; i++) {
                    dati = 0x0;
                    if(vmm->ReadSafe(addr,&dati) == 0) {
                        std::printf("read data 0x%08x from address 0x%06x\n",dati,addr);
                    }
                    else {
                        std::printf("bus ERROR reading from address 0x%06x\n",addr);
                    }
                    addr += incr;
                }
            }
        }

        // read CR/CSR ---------------------------------------------------------
        else if(op == "rc") {
            unsigned int dwid = addr & DWID_CRCSR_MASK;
            if((dwid != CRCSR_D08) and (dwid != CRCSR_D16) and (dwid != CRCSR_D32)) {
                std::printf("ERROR - illegal CR/CSR data width \"0x%08x\"\n",addr);
                continue;
            }
            if((addr&(~DWID_CRCSR_MASK)) > ADDR_CRCSR_MAX) {
                std::printf("ERROR - illegal CR/CSR offset \"0x%08x\"\n",addr);
                continue;
            }
            addr |= ((dwid == CRCSR_D08) ? 0x10000000 : ((dwid == CRCSR_D16) ? 0x20000000 : 0x40000000));
            if(loop < 1 || loop > 21) {
                std::printf("ERROR - wrong slot number \"%d\"\n",loop);
                continue;
            }
            slot = loop;
            offs = dwid | addr;
            dati = 0x0;
            if(arg.size() > 4) {
                if(sscanf(arg[4].c_str(),"%d",&loop) != 1) {
                    std::printf("ERROR scanning <step> from command\n");
                    continue;
                }
                if((loop < 0) || (loop > 4)) {
                    std::printf("ERROR - step value \"%d\"\n",incr);
                    continue;
                }
            }
            else {
                loop = 1;
            }
            for(i=0; i<loop; i++) {
                if(vme->ReadCRCSR(slot,offs,&dati) == 0) {
                    unsigned int dlen = (dwid == CRCSR_D08) ? 2 : ((dwid == CRCSR_D16) ? 4 : 8);
                    std::printf("read CR/CSR data 0x%0*x from offset 0x%05x in slot %2d\n",dlen,dati,offs&ADDR_CRCSR_MAX,slot);
                }
                else {
                    std::printf("ERROR reading CR/CSR from offset 0x%05x in slot %2d\n",offs&ADDR_CRCSR_MAX,slot);
                }
                offs += 0x00000004;
            }
        }

        // read block ----------------------------------------------------------
        else if(op == "rb") {
            if(addr >= g_size) {
                std::printf("ERROR - illegal address \"0x%08x\"\n",addr);
                continue;
            }
            if(loop > (int)g_size || loop > (int)g_bsiz) {
                 std::printf("ERROR - illegal size \"%d\"\n",loop);
                 continue;
            }
            if(tvme_block(addr,loop,VME_DMA_D32R) == 0) {
                std::printf("read block from address 0x%06x, size %d\n",addr,loop);
                tvme_bdump(loop);
            }
            else {
                std::printf("ERROR reading block from address 0x%06x, size %d\n",addr,loop);
                continue;
            }
            if(arg.size() > 4) {
                if(sscanf(arg[4].c_str(),"%x",&incr) != 1) {
                    if(tvme_wfile(arg[4],loop) != 0) {
                        std::printf("ERROR writing data of size %d to output file \"%s\"\n",loop,arg[4].c_str());
                    }
                }
            }
        }

        // loop read block -----------------------------------------------------
        else if(op == "lr") {
            if(addr >= g_size) {
                std::printf("ERROR - illegal address \"0x%08x\"\n",addr);
                continue;
            }
            if(loop > (int)g_size || loop > (int)g_bsiz) {
                 std::printf("ERROR - illegal size \"%d\"\n",loop);
                 continue;
            }
            double tstart, ftim, freq;
            long long unsigned int tnum(0), tdat;
            tstart = tvme_gtime();
            while(true) {
                if(tvme_block(addr,loop,VME_DMA_D32R) != 0) {
                    std::printf("ERROR reading block from address 0x%06x, size %d\n",addr,loop);
                    break;
                }
                if((++tnum)%g_prnt == 0) {
                    ftim = tvme_gtime() - tstart;
                    std::cout << "********************************************************************************" << std::endl;
                    std::printf("   time since start               : %16.3f s\n",ftim);
                    freq = static_cast<double>(tnum)/ftim;
                    std::printf("   number of blocks read          : %16lld => %s\n",tnum,tvme_gfreq(freq).c_str());
                    tdat = tnum*loop;
                    freq = static_cast<double>(tdat*sizeof(uint32_t))/ftim;
                    std::printf("   number of words read           : %16lld => %s\n",tdat,tvme_grate(freq).c_str());
                }
            }
        }

        // write short ---------------------------------------------------------
        else if(op == "w") {
            if(addr >= g_size) {
                std::printf("ERROR - illegal address \"0x%08x\"\n",addr);
                continue;
            }
            if(arg.size() > 4) {
                if(sscanf(arg[4].c_str(),"%x",&incr) != 1) {
                    std::printf("ERROR scanning <incr> from command\n");
                    continue;
                }
            }
            if(loop == -1) {
                dats = (u_short)data;
                while(true) {
                    if(vmm->WriteSafe(addr,dats) == 0) {
                        std::printf("wrote data 0x%04x to address 0x%06x\n",dats,addr);
                    }
                    else {
                        std::printf("bus ERROR writing to address 0x%06x\n",addr);
                    }
                    addr += incr;
                }
            }
            else {
                eflg = 0;
                dats = (u_short)data;
                for(i=0; i<loop; i++) {
                    if(vmm->WriteSafe(addr,dats) == 0) {
                    }
                    else {
                        eflg++;
                        std::printf("bus ERROR writing to address 0x%06x\n",addr);
                    }
                    addr += incr;
                }
                std::printf("wrote data 0x%04x to address 0x%06x %d times\n",dats,addr,loop-eflg);
            }
        }

        // write long ----------------------------------------------------------
        else if(op == "wl") {
            if(addr >= g_size) {
                std::printf("ERROR - illegal address \"0x%08x\"\n",addr);
                continue;
            }
            if(arg.size() > 4) {
                if(sscanf(arg[4].c_str(),"%x",&incr) != 1) {
                    std::printf("ERROR scanning <incr> from command\n");
                    continue;
                }
            }
            if(loop == -1) {
                dati = data;
                while(true) {
                    if(vmm->WriteSafe(addr,dati) == 0) {
                        std::printf("wrote data 0x%08x to address 0x%06x\n",dati,addr);
                    }
                    else {
                        std::printf("bus ERROR writing to address 0x%06x\n",addr);
                    }
                    addr += incr;
                }
            }
            else {
                eflg = 0;
                dati = data;
                for(i=0; i<loop; i++) {
                    if(vmm->WriteSafe(addr,dati) == 0) {
                    }
                    else {
                        eflg++;
                        std::printf("bus ERROR writing to address 0x%06x\n",addr);
                    }
                    addr += incr;
                }
                std::printf("wrote data 0x%08x to address 0x%06x %d times\n",data,addr,loop-eflg);
            }
        }

        // write CR/CSR --------------------------------------------------------
        else if(op == "wc") {
            unsigned int dwid = addr & DWID_CRCSR_MASK;
            if((dwid != CRCSR_D08) and (dwid != CRCSR_D16) and (dwid != CRCSR_D32)) {
                std::printf("ERROR - illegal CR/CSR data width \"0x%08x\"\n",addr);
                continue;
            }
            if((addr&(~DWID_CRCSR_MASK)) > ADDR_CRCSR_MAX) {
                std::printf("ERROR - illegal CR/CSR offset \"0x%08x\"\n",addr);
                continue;
            }
            addr |= ((dwid == CRCSR_D08) ? 0x10000000 : ((dwid == CRCSR_D16) ? 0x20000000 : 0x40000000));
            if(loop < 1 || loop > 21) {
                std::printf("ERROR  wrong slot number \"%d\"\n",loop);
                continue;
            }
            slot = loop;
            offs = dwid | addr;
            dati = data;
            if(arg.size() > 4) {
                if(sscanf(arg[4].c_str(),"%d",&loop) != 1) {
                    std::printf("ERROR scanning <step> from command\n");
                    continue;
                }
                if((loop < 0) || (loop > 4)) {
                    std::printf("ERROR - step value \"%d\"\n",incr);
                    continue;
                }
            }
            else {
                loop = 1;
            }
            for(i=0; i<loop; i++) {
                if(vme->WriteCRCSR(slot,offs,dati) == 0) {
                    unsigned int dlen = (dwid == CRCSR_D08) ? 2 : ((dwid == CRCSR_D16) ? 4 : 8);
                    std::printf("wrote CR/CSR data 0x%0*x to offset 0x%05x in slot %2d\n",dlen,dati,offs&ADDR_CRCSR_MAX,slot);
                }
                else {
                    std::printf("ERROR writing CR/CSR data to offset 0x%05x in slot %2d\n",offs&ADDR_CRCSR_MAX,slot);
                }
                dati += 0x00000004;
            }
        }

        // write block ----------------------------------------------------------
        else if(op == "wb") {
            if(addr >= g_size) {
                std::printf("ERROR - illegal address \"0x%08x\"\n",addr);
                continue;
            }
            if(loop > (int)g_size || loop > (int)g_bsiz) {
                 std::printf("ERROR - illegal size \"%d\"\n",loop);
                 continue;
            }
            if(arg.size() > 4) {
                if(sscanf(arg[4].c_str(),"%x",&incr) != 1) {
                    if(tvme_rfile(arg[4],loop) != 0) {
                        std::printf("ERROR reading data of size %d from input file \"%s\"\n",loop,arg[4].c_str());
                        continue;
                    }
                }
                else {
                     tvme_bfill(data,loop,incr);
                }
            }
            else {
                tvme_bfill(data,loop,0);
            }
            if(tvme_block(addr,loop,VME_DMA_D32W) == 0) {
                std::printf("wrote block to address 0x%06x, size %d\n",addr,loop);
            }
            else {
                std::printf("ERROR writing block to address 0x%06x, size %d\n",addr,loop);
            }
        }

        // loop write block -----------------------------------------------------
        else if(op == "lw") {
            if(addr >= g_size) {
                std::printf("ERROR - illegal address \"0x%08x\"\n",addr);
                continue;
            }
            if(loop > (int)g_size || loop > (int)g_bsiz) {
                 std::printf("ERROR - illegal size \"%d\"\n",loop);
                 continue;
            }
            if(arg.size() > 4) {
                if(sscanf(arg[4].c_str(),"%x",&incr) != 1) {
                    if(tvme_rfile(arg[4],loop) != 0) {
                        std::printf("ERROR reading data of size %d from input file \"%s\"\n",loop,arg[4].c_str());
                        continue;
                    }
                }
                else {
                     tvme_bfill(data,loop,incr);
                }
            }
            else {
                tvme_bfill(data,loop,incr);
            }
            double tstart, ftim, freq;
            long long unsigned int tnum(0), tdat;
            tstart = tvme_gtime();
            while(true) {
                if(tvme_block(addr,loop,VME_DMA_D32W) != 0) {
                    std::printf("ERROR writing block from address 0x%06x, size %d\n",addr,loop);
                    break;
                }
                if((++tnum)%g_prnt == 0) {
                    ftim = tvme_gtime() - tstart;
                    std::cout << "********************************************************************************" << std::endl;
                    std::printf("   time since start               : %16.3f s\n",ftim);
                    freq = static_cast<double>(tnum)/ftim;
                    std::printf("   number of blocks written       : %16lld => %s\n",tnum,tvme_gfreq(freq).c_str());
                    tdat = tnum*loop;
                    freq = static_cast<double>(tdat*sizeof(uint32_t))/ftim;
                    std::printf("   number of words written        : %16lld => %s\n",tdat,tvme_grate(freq).c_str());
                }
            }
        }

        // quit ----------------------------------------------------------------
        else if(op == "q") {
            break;
        }

        // help ----------------------------------------------------------------
        else if(op == "h") {
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
            std::cout << "\033[1mr/rl <1> <2> <3> <4>\033[0m = read or read long:" << std::endl;
            std::cout << "   <1> = address/hex <2> = unused (set to \"0\") <3> = loop/int (-1=inf) <4> = address increment/hex" << std::endl;
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
            std::cout << "\033[1mw/wl <1> <2> <3> <4>\033[0m = write or write long:" << std::endl;
            std::cout << "   <1> = address/hex <2> = data/hex            <3> = loop/int (-1=inf) <4> = address increment/hex" << std::endl;
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
            std::cout << "\033[1mrc <1> <2> <3> <4>\033[0m = read CR/CSR:" << std::endl;
            std::cout << "   <1> = offset/hex (0x000xxxxx = D8, 0x010xxxxx = D16, 0x020xxxxx = D32)" << std::endl;
            std::cout << "                     <2> = unused (set to \"0\") <3> = slot/int          <4> = steps/int (max=4)" << std::endl;
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
            std::cout << "\033[1mwc <1> <2> <3> <4>\033[0m = write CR/CSR:" << std::endl;
            std::cout << "   <1> = offset/hex (0x000xxxxx = D8, 0x010xxxxx = D16, 0x020xxxxx = D32)" << std::endl;
            std::cout << "                     <2> = data/hex            <3> = slot/int          <4> = steps/int (max=4)" << std::endl;
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
            std::cout << "\033[1mrb <1> <2> <3> <4>\033[0m = read block:" << std::endl;
            std::cout << "   <1> = address/hex <2> = unused (set to \"0\") <3> = size/int (D32)    <4> = output file/if not number" << std::endl;
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
            std::cout << "\033[1mwb <1> <2> <3> <4>\033[0m = write block:" << std::endl;
            std::cout << "   <1> = address/hex <2> = data/hex            <3> = size/int (D32)    <4> = data increment/hex or input file/if not number" << std::endl;
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
            std::cout << "\033[1mlr <1> <2> <3> <4>\033[0m = loop read block:" << std::endl;
            std::cout << "   <1> = address/hex <2> = unused (set to \"0\") <3> = size/int (D32)" << std::endl;
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
            std::cout << "\033[1mlw <1> <2> <3> <4>\033[0m = loop write block:" << std::endl;
            std::cout << "   <1> = address/hex <2> = data/hex            <3> = size/int (D32)    <4> = data increment/hex or input file/if not number" << std::endl;
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
            std::cout << "\033[1mq\033[0m = quit program" << std::endl;
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
            std::cout << "\033[1mh\033[0m = print this help" << std::endl;
            std::cout << "---------------------------------------------------------------------------------------------------------------------------" << std::endl;
        }

        // unrecognised command ------------------------------------------------
        else {
            std::cout << "UNRECOGNISED command\n";
        }

#ifndef TERMIOS
        free(input);
#endif  // TERMIOS
    }

    // delete contiguous memory segment
    delete seg;

    // unmap VME master mapping
    vme->MasterUnmap(vmm);

    // close VME library
    VME::Close();

    std::exit(0);
}

//------------------------------------------------------------------------------

int tvme_block(const u_int addr, const u_int size, const u_int dir) {

    return(vme->RunBlockTransfer(*seg,0x00000000,g_base+addr,size,dir,0xffffffff,false));
}

//------------------------------------------------------------------------------

int tvme_bdump(const u_int size) {

    uint32_t* data = (uint32_t*)seg->VirtualAddress();
    u_int i;

    // dump general information
    std::printf("CMEM segment, virt = %p, phys = 0x%016lx, size = %d\n",(void*)data,seg->PhysicalAddress(),seg->Size());
    if(size > (seg->Size()/sizeof(uint32_t))) {
        CERR("illegal size %d",size);
        return(-1);
    }

    // dump data
    for(i=0; i<size; i++) {
        if(i%8 == 0) std::printf("%6d:",i);
        std::printf(" 0x%08x",data[i]);
        if(i%8 == 7) std::cout << std::endl;
    }
    if(i%8 != 0) std::cout << std::endl;

    return(0);
}

//------------------------------------------------------------------------------

int tvme_bfill(const u_int word, const u_int size, const u_int incr) {

    uint32_t* data = (uint32_t*)seg->VirtualAddress();
    u_int i, w(word);

    // fill data
    for(i=0; i<size; i++) {
        data[i] = w;
        w += incr;
    }

    return(0);
}

//------------------------------------------------------------------------------

int tvme_rfile(const std::string& fn, const unsigned int size) {

    // open input file
    std::ifstream inf(fn.c_str()); if(!inf) {
        CERR("cannot open input file \"%s\"",fn.c_str());
        return(-1);
    }

    unsigned int value, osiz;
    std::string buf;
    uint32_t* data = (uint32_t*)seg->VirtualAddress();

    // read from file
    for(osiz=0; osiz<size; osiz++) {
        if(!(inf >> buf)) {
//            if(inf.eof()) break;
            CERR("reading word %d from input file \"%s\"",osiz,fn.c_str());
            return(-1);
        }
        if(buf[0] == '#') {
            if(!getline(inf,buf)) {
                CERR("reading comment before word %d from input file \"%s\"",osiz,fn.c_str());
                return(-1);
            }
            osiz--;
            continue;
        }
        if(sscanf(buf.c_str(),"%x",&value) != 1) {
            CERR("scanning word %d from input file \"%s\"",osiz,fn.c_str());
            return(-1);
        }
        data[osiz] = value;
    }
    COUT("read %d data words from input file \"%s\"",osiz,fn.c_str());

    // close input file
    inf.close();

    return(0);
}

//------------------------------------------------------------------------------

int tvme_wfile(const std::string& fn, const unsigned int size) {

    // open output file
    std::ofstream out(fn.c_str()); if(! out) {
        CERR("cannot open output file \"%s\"",fn.c_str());
        return(-1);
    }

    unsigned int i;
    uint32_t* data = (uint32_t*)seg->VirtualAddress();

    // write to file
    out << std::hex << std::setfill('0');
    for(i=0;  i<size; i++) {
        if(!(out << "0x" << std::setw(8) << data[i] << std::endl)) {
            CERR("writing word %d into output file \"%s\"",i,fn.c_str());
            return(-1);
        }
    }
    COUT("wrote %d data words into output file \"%s\"",size,fn.c_str());

    // close output file
    out.close();

    return(0);
}

//------------------------------------------------------------------------------

double tvme_gtime() {

    struct timeval tv;

    gettimeofday(&tv,0);
    return(static_cast<double>(tv.tv_sec) + static_cast<double>(tv.tv_usec)/1000000.0);
}

//------------------------------------------------------------------------------

std::string tvme_gfreq(const double freq) {

    char buf[1024];

    if(freq < 1.0e3) {
        std::sprintf(buf,"%7.3f  Hz",freq);
    }
    else if(freq >= 1.0e3 && freq < 1.0e6) {
        std::sprintf(buf,"%7.3f kHz",freq/1.0e3);
    }
    else if(freq >= 1.0e6 && freq < 1.0e9) {
        std::sprintf(buf,"%7.3f MHz",freq/1.0e6);
    }
    else if(freq >= 1.0e9 && freq < 1.0e12) {
        std::sprintf(buf,"%7.3f GHz",freq/1.0e9);
    }
    else {
        std::sprintf(buf,"%10.3e GHz",freq/1.0e9);
    }

    return(std::string(buf));
}

//------------------------------------------------------------------------------

std::string tvme_grate(const double rate) {

    char buf[1024];

    if(rate < 1024.0) {
        std::sprintf(buf,"%7.3f  Byte/s",rate);
    }
    else if(rate >= 1.024e3 && rate < 1.048576e6) {
        std::sprintf(buf,"%7.3f kByte/s",rate/1.024e3);
    }
    else if(rate >= 1.048576e6 && rate < 1.073741824e9) {
        std::sprintf(buf,"%7.3f MByte/s",rate/1.048576e6);
    }
    else if(rate >= 1.073741824e9 && rate < 1.099511627776e12) {
        std::sprintf(buf,"%7.3f GByte/s",rate/1.073741824e9);
    }
    else {
        std::sprintf(buf,"%10.3e GByte/s",rate/1.073741824e9);
    }

    return(std::string(buf));
}

//------------------------------------------------------------------------------

int tvme_split(const std::string& str, std::vector<std::string>& rslt) {

    rslt.clear();

    if(str.empty()) return(0);

    static const std::string del = " ";
    std::string::size_type idx0(0), idx1(0);

    while(true) {
        idx0 = str.find_first_not_of(del,idx0);
        if(idx0 == std::string::npos) break;
        idx1 = str.find_first_of(del,idx0);
        if(idx1 != std::string::npos) {
            rslt.push_back(str.substr(idx0,idx1-idx0));
        }
        else {
            rslt.push_back(str.substr(idx0));
            break;
        }
        idx0 = idx1+1;
    }

    return(rslt.size());
}

#ifdef TERMIOS
//------------------------------------------------------------------------------

int tvme_input(std::string& str) {

    tvme_canon(false);

    str.clear();

    int key;
    unsigned int pos(0), idx(0), i;

    static TVME_BUFFER buf(g_hist);

    std::string stq;

    while(true) {

        // read character
        key = fgetc(stdin);
        CDBG("\nkey[0] = %d\n",key);

        // capture BACKSPACE (depends on terminal settings?!)
        if(key == 8) {
            CDBG("\nlen = %d, pos = %d\n",str.length(),pos);
            if((pos+3) > str.length()) for(i=0; i<(pos+3-str.length()); i++) std::printf("\b \b");
            if((pos > 0) && (pos <= str.length())) pos--;
            str.erase((std::string::size_type)pos,1);
            std::printf("\r>>> %s",str.c_str());
            CDBG("\nlen = %d, pos = %d\n",str.length(),pos);
            for(i=0; i<(str.length()-pos); i++) std::printf("\b");
        }

        // capture ARROWS and DELETE (depends on terminal settings?!)
        else if(key == 27) {
            key = fgetc(stdin);
            CDBG("\nkey[1] = %d\n",key);

            if(key == 91) {
                key = fgetc(stdin);
                CDBG("\nkey[2] = %d\n",key);

                // ARROW UP
                if(key == 65) {
                    std::printf("\b\b\b\b    \b\b\b\b");
                    if(idx == 0) stq = str;
                    if(idx < buf.fill()) {
                        idx++;
                        if(buf.fill() != 0) {
                            if(buf.getPrev(idx,str) != TVME_BUFFER::SUCCESS) {
                                CERR("calling TVME_BUFFER::getPrev with index %d\n",idx);
                                return(-1);
                            }
                            if((pos) > str.length()) for(i=0; i<(pos-str.length()); i++) std::printf("\b \b");
                            std::printf("\r>>> %s",str.c_str());
                            pos = str.length();
                        }
                    }
                }

                // ARROW DOWN
                if(key == 66) {
                    std::printf("\b\b\b\b    \b\b\b\b");
                    if(idx > 0) {
                        idx--;
                        if(idx > 0) {
                            if(buf.fill() != 0) {
                                if(buf.getPrev(idx,str) != TVME_BUFFER::SUCCESS) {
                                    CERR("calling TVME_BUFFER::getPrev with index %d\n",idx);
                                    return(-1);
                                }
                            }
                        }
                        else {
                            str = stq;
                        }
                        if(pos > str.length()) for(i=0; i<(pos-str.length()); i++) std::printf("\b \b");
                        std::printf("\r>>> %s",str.c_str());
                        pos = str.length();
                    }
                }

                // ARROW RIGHT
                else if(key == 67) {
                    if((pos+4) > str.length()) for(i=0; i<(pos+4-str.length()); i++) std::printf("\b \b");
                    if(pos < str.length()) pos++;
                    std::printf("\r>>> %s",str.c_str());
                    for(i=0; i<(str.length()-pos); i++) std::printf("\b");
                }

                // ARROW LEFT
                else if(key == 68) {
                    if((pos+4) > str.length()) for(i=0; i<(pos+4-str.length()); i++) std::printf("\b \b");
                    if((pos > 0) && (pos <= str.length())) pos--;
                    std::printf("\r>>> %s",str.c_str());
                    for(i=0; i<(str.length()-pos); i++) std::printf("\b");
                }

                // DELETE
                if(key == 51) {
                    key = fgetc(stdin);
                    CDBG("\nkey[3] = %d\n",key);

                    if(key == 126) {
                        CDBG("\nlen = %d, pos = %d\n",str.length(),pos);
                        if((pos+5) > str.length()) for(i=0; i<(pos+5-str.length()); i++) std::printf("\b \b");
                        if(pos <= str.length()) str.erase((std::string::size_type)pos,1);
                        std::printf("\r>>> %s",str.c_str()); std::printf(" \b");
                        CDBG("\nlen = %d, pos = %d\n",str.length(),pos);
                        for(i=0; i<(str.length()-pos); i++) std::printf("\b");
                    }
                }
            }
        }

        // END OF LINE
        else if(key == 10) {
            if(buf.putNext(str) != TVME_BUFFER::SUCCESS) {
                CERR("calling TVME_BUFFER::putNext\n","");
                return(-1);
            }
            return(0);
        }

        // any other character
        else {
            str.insert((std::string::size_type)pos,1,key);
            pos++;
            std::printf("\r>>> %s",str.c_str());
            for(i=0; i<(str.length()-pos); i++) std::printf("\b");
        }
    }

    return(0);
}

//------------------------------------------------------------------------------

void tvme_canon(bool canon) {

    struct termios ttystate;

    // get the terminal attributes
    tcgetattr(STDIN_FILENO, &ttystate);

    if(canon) {

        // turn on canonical mode
        ttystate.c_lflag |= ICANON;
    }
    else {

        // turn off canonical mode
        ttystate.c_lflag &= ~ICANON;

        // minimum of number input read
        ttystate.c_cc[VMIN] = 1;
    }

    // set the terminal attributes
    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}
#endif  // TERMIOS
