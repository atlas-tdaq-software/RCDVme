//******************************************************************************
// file: menuRCDVme.cc
// desc: menu for test of VMEbus C++ wrapper library
// auth: 17/09/01 R. Spiwoks
// modf: 02/12/02 R. Spiwoks, DataFlow repository
// modf: 19/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
// modf: 04/05/10 E. Pasqualucci: compiled on 64 bits machine - DOES NOT MEAN that it works!!!!
//******************************************************************************

// $Id$

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <signal.h>
#include <cstdio>
#include <stdint.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include "DFDebug/DFDebug.h"
#include "RCDMenu/RCDMenu.h"
#include "RCDVme/RCDVme.h"
#include "RCDVme/RCDCmemSegment.h"

using namespace RCD;

VME*                    vme;
VMEMasterMap*           vmm;
VMESlaveMap*            vsm;
VMEBlockTransfer*       blk;
VMEInterrupt*           irq;

CMEMSegment*            buf;
int                     event_size;

//------------------------------------------------------------------------------

void bus_error_handler(int ) {

    static VMEBusErrorInfo error_info;
    static u_int rtnv;

    if((rtnv = vme->BusErrorInfoGet(error_info)) != VME_SUCCESS) {
      ERR_TEXT(DFDB_RCDVME,"");
      vme->ErrorPrint(rtnv);
    }
    else {
      DEBUG_TEXT(DFDB_RCDVME,20,"addr = " << std::hex << std::setfill('0') << std::setw(8) << error_info.vmebus_address
                 << ", am = " << std::setw(2) << error_info.address_modifier
                 << ", mult = " << error_info.multiple << std::dec);

    }
}

//------------------------------------------------------------------------------

void interrupt_handler(int ) {

    static VMEInterruptInfo interrupt_info;
    static u_int rtnv;

    // get interrupt information
    if((rtnv = irq->InfoGet(interrupt_info)) != VME_SUCCESS) {
        ERR_TEXT(DFDB_RCDVME,"getting interrupt information");
        vme->ErrorPrint(rtnv);
    }
    else {
      DEBUG_TEXT(DFDB_RCDVME,20,"vector = " << std::hex << std::setfill('0') << std::setw(2) << interrupt_info.vector
                 << ", level = " << std::dec << interrupt_info.level
                 << ", mult = " << std::hex << interrupt_info.multiple << std::dec);
    }

    // re-enable interrupt
    if((rtnv = irq->Reenable()) != VME_SUCCESS) {
        ERR_TEXT(DFDB_RCDVME,"re-enabling interrupt");
        vme->ErrorPrint(rtnv);
    }
    else {
      DEBUG_TEXT(DFDB_RCDVME,15,"interrupt re-enabled");
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class ItemOpen : public MenuItem {
  public:
    ItemOpen() { setName("Open"); }
    int action() {

        vme = VME::Open();
        buf = new CMEMSegment("buf",0x10000);

        return((*vme)());
    }
};

//------------------------------------------------------------------------------

class ItemClose : public MenuItem {
  public:
    ItemClose() { setName("Close"); }
    int action() {

        delete buf;
        VME::Close();

        return(0);
    }
};

//------------------------------------------------------------------------------

class ItemBusErrorRegisterSignal : public MenuItem {
  public:
    ItemBusErrorRegisterSignal() { setName("RegisterSignal"); }
    int action() {

        struct sigaction sa;

        int s = enterInt("signal",0,256);

        sa.sa_handler = (void (*)(int))bus_error_handler;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        if(sigaction(s,&sa,(struct sigaction *)0)) {
            ERR_TEXT(DFDB_RCDVME,"installing bus error handler");
            return(-1);
        }
        return(vme->BusErrorRegisterSignal(s));
    }
};

//------------------------------------------------------------------------------

class ItemBusErrorInfoGet : public MenuItem {
  public:
    ItemBusErrorInfoGet() { setName("InfoGet"); }
    int action() {

        VMEBusErrorInfo error_info;
        u_int rtnv;

        if((rtnv = vme->BusErrorInfoGet(error_info)) != VME_SUCCESS) {
            ERR_TEXT(DFDB_RCDVME,"");
            vme->ErrorPrint(rtnv);
        }
        else {
          DEBUG_TEXT(DFDB_RCDVME,20,"addr = " << std::hex << std::setfill('0') << std::setw(8) << error_info.vmebus_address
                     << ", am = " << std::setw(2) << error_info.address_modifier
                     << ", mult = " << error_info.multiple << std::dec);
        }
        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ItemMasterMap : public MenuItem {
  public:
    ItemMasterMap() { setName("MasterMap"); }
    int action() {

        u_int p = (u_int)enterHex("vmebus_address   (hex)",0,0xffffffff);
        if(p < 0x100) p = p << 24;
        u_int s = (u_int)enterHex("window_size      (hex)",0,0xffffffff);
        u_int a = (u_int)enterHex("address_modifier (hex)",0,0xffffffff);
        u_int o = (u_int)enterHex("options          (hex)",0,0xffffffff);

        vmm = vme->MasterMap(p,s,a,o);
        return((*vmm)());
    }
};

//------------------------------------------------------------------------------

class ItemRead : public MenuItem {
  public:
    ItemRead() { setName("Read"); }
    int action() {

        u_int offs = enterHex("address_offset         (hex)",0,0xffffffff);
        int type = enterInt("u_int=0, u_short=1, u_char=2",0,2);
        int mode = enterInt("safe=0, fast=1              ",0,1);
        int loop = enterInt("loop (-1=inf)               ",-1,2147148300);

        int i;
        u_int dati;
        u_short dats;
        u_char datb;

        if(type == 0) {
          if(mode == 0) {
            if(loop == -1) {
              while(1) {
                if(vmm->ReadSafe(offs,&dati) == 0) {
                  std::printf("ItemRead: read data %08x from offs %08x\n",dati,offs);
                }
                else {
                  std::printf("ItemRead: bus ERROR reading from offs %08x\n",offs);
                }
              }
            }
            else {
              for(i=0; i<loop; i++) {
                if(vmm->ReadSafe(offs,&dati) == 0) {
                  std::printf("ItemRead: read data %08x from offs %08x\n",dati,offs);
                }
                else {
                  std::printf("ItemRead: bus ERROR reading from offs %08x\n",offs);
                }
              }
            }
          }
          else if(mode == 1) {
            if(loop == -1) {
              while(1) {
                vmm->ReadFast(offs,&dati);
                std::printf("ItemRead: read data %08x from offs %08x\n",dati,offs);
              }
            }
            else {
              for(i=0; i<loop; i++) {
                vmm->ReadFast(offs,&dati);
                std::printf("ItemRead: read data %08x from offs %08x\n",dati,offs);
              }
            }
          }
        }
        else if(type == 1) {
          if(mode == 0) {
            if(loop == -1) {
              while(1) {
                if(vmm->ReadSafe(offs,&dats) == 0) {
                  std::printf("ItemRead: read data %04x from offs %08x\n",dats,offs);
                }
                else {
                  std::printf("ItemRead: bus ERROR reading from offs %08x\n",offs);
                }
              }
            }
            else {
              for(i=0; i<loop; i++) {
                if(vmm->ReadSafe(offs,&dats) == 0) {
                  std::printf("ItemRead: read data %04x from offs %08x\n",dats,offs);
                }
                else {
                  std::printf("ItemRead: bus ERROR reading from offs %08x\n",offs);
                }
              }
            }
          }
          else if(mode == 1) {
            if(loop == -1) {
              while(1) {
                vmm->ReadFast(offs,&dats);
                std::printf("ItemRead: read data %04x from offs %08x\n",dats,offs);
              }
            }
            else {
              for(i=0; i<loop; i++) {
                vmm->ReadFast(offs,&dats);
                std::printf("ItemRead: read data %04x from offs %08x\n",dats,offs);
              }
            }
          }
        }
        else if(type == 2) {
          if(mode == 0) {
            if(loop == -1) {
              while(1) {
                if(vmm->ReadSafe(offs,&datb) == 0) {
                  std::printf("ItemRead: read data %04x from offs %08x\n",datb,offs);
                }
                else {
                  std::printf("ItemRead: bus ERROR reading from offs %08x\n",offs);
                }
              }
            }
            else {
              for(i=0; i<loop; i++) {
                if(vmm->ReadSafe(offs,&datb) == 0) {
                  std::printf("ItemRead: read data %04x from offs %08x\n",datb,offs);
                }
                else {
                  std::printf("ItemRead: bus ERROR reading from offs %08x\n",offs);
                }
              }
            }
          }
          else if(mode == 1) {
            if(loop == -1) {
              while(1) {
                vmm->ReadFast(offs,&datb);
                std::printf("ItemRead: read data %04x from offs %08x\n",datb,offs);
              }
            }
            else {
              for(i=0; i<loop; i++) {
                vmm->ReadFast(offs,&datb);
                std::printf("ItemRead: read data %04x from offs %08x\n",datb,offs);
              }
            }
          }
        }

        return(0);
    }
};

//------------------------------------------------------------------------------

class ItemWrite : public MenuItem {
  public:
    ItemWrite() { setName("Write"); }
    int action() {

        u_int offs = enterHex("address_offset (hex)",0,0xffffffff);
        u_int data = enterHex("data           (hex)",0,0xffffffff);
        int type = enterInt("u_int=0, u_short=1    ",0,1);
        int mode = enterInt("safe=0, fast=1      ",0,1);
        int loop = enterInt("loop (-1=inf)       ",-1,2147148300);

        int i;

        if(type == 0) {
          if(mode == 0) {
            if(loop == -1) {
              while(1) {
                if(vmm->WriteSafe(offs,data) == 0) {
                  std::printf("ItemWrite: wrote data %08x to offs %08x\n",data,offs);
                }
                else {
                  std::printf("ItemWrite: bus ERROR writing to offs %08x\n",offs);
                }
              }
            }
            else {
              for(i=0; i<loop; i++) {
                if(vmm->WriteSafe(offs,data) == 0) {
                  std::printf("ItemWrite: wrote data %08x to offs %08x\n",data,offs);
                }
                else {
                  std::printf("ItemWrite: bus ERROR writing to offs %08x\n",offs);
                }
              }
            }
          }
          else if(mode == 1) {
            if(loop == -1) {
              while(1) {
                vmm->WriteFast(offs,data);
                std::printf("ItemWrite: wrote data %08x to offs %08x\n",data,offs);
              }
            }
            else {
              for(i=0; i<loop; i++) {
                vmm->WriteFast(offs,data);
                std::printf("ItemWrite: wrote data %08x to offs %08x\n",data,offs);
              }
            }
          }
        }
        else if(type == 1) {
          if(mode == 0) {
            if(loop == -1) {
              while(1) {
                if(vmm->WriteSafe(offs,data) == 0) {
                  std::printf("ItemWrite: wrote data %04x to offs %08x\n",data,offs);
                }
                else {
                  std::printf("ItemWrite: bus ERROR writing to offs %08x\n",offs);
                }
              }
            }
            else {
              for(i=0; i<loop; i++) {
                if(vmm->WriteSafe(offs,data) == 0) {
                  std::printf("ItemWrite: wrote data %04x to offs %08x\n",data,offs);
                }
                else {
                  std::printf("ItemWrite: bus ERROR writing to offs %08x\n",offs);
                }
              }
            }
          }
          else if(mode == 1) {
            if(loop == -1) {
              while(1) {
                vmm->WriteFast(offs,data);
                std::printf("ItemWrite: wrote data %04x to offs %08x\n",data,offs);
              }
            }
            else {
              for(i=0; i<loop; i++) {
                vmm->WriteFast(offs,data);
                std::printf("ItemWrite: wrote data %04x to offs %08x\n",data,offs);
              }
            }
          }
        }

        return(0);
    }
};

//------------------------------------------------------------------------------

class ItemVirtualAddress : public MenuItem {
  public:
    ItemVirtualAddress() { setName("VirtualAddress"); }
    int action() {

        u_long virt;

        if(vmm->VirtualAddress(&virt) != VME_SUCCESS) {
            ERR_TEXT(DFDB_RCDVME,"reading virtual address");
        }
        else {
          DEBUG_TEXT(DFDB_RCDVME,20,"virt = " << std::hex << std::setfill('0') << std::setw(16) << virt << std::dec);
        }

        return(0);
    }
};

//------------------------------------------------------------------------------

class ItemMasterMapDump : public MenuItem {
  public:
    ItemMasterMapDump() { setName("Dump"); }
    int action() {

        return(vmm->Dump());
    }
};

//------------------------------------------------------------------------------

class ItemMasterMapsDump : public MenuItem {
  public:
    ItemMasterMapsDump() { setName("MasterMapDump"); }
    int action() {

        return(vme->MasterMapDump());
    }
};

//------------------------------------------------------------------------------

class ItemMasterUnmap : public MenuItem {
  public:
    ItemMasterUnmap() { setName("MasterUnmap"); }
    int action() {

        return(vme->MasterUnmap(vmm));
    }
};

//------------------------------------------------------------------------------

class ItemSlaveMap : public MenuItem {
  public:
    ItemSlaveMap() { setName("SlaveMap"); }
    int action() {

        u_int p = (u_int)enterHex("system_iobus_address (hex)",0,0xffffffff);
        if(p < 0x100) p = p << 24;
        u_int s = (u_int)enterHex("window_size          (hex)",0,0xffffffff);
        u_int a = (u_int)enterHex("address_width        (hex)",0,0xffffffff);
        u_int o = (u_int)enterHex("options              (hex)",0,0xffffffff);

        vsm = vme->SlaveMap(p,s,a,o);
        return((*vsm)());
    }
};

//------------------------------------------------------------------------------

class ItemVmebusAddress : public MenuItem {
  public:
    ItemVmebusAddress() { setName("VmebusAddress"); }
    int action() {

        u_int vme;

        if(vsm->VmebusAddress(&vme) != VME_SUCCESS) {
            ERR_TEXT(DFDB_RCDVME,"reading VMEbus address");
        }
        else {
          DEBUG_TEXT(DFDB_RCDVME,20,"vme = " << std::hex << std::setfill('0') << std::setw(8) << vme << std::dec);
        }

        return(0);
    }
};

//------------------------------------------------------------------------------

class ItemSlaveUnmap : public MenuItem {
  public:
    ItemSlaveUnmap() { setName("SlaveUnmap"); }
    int action() {

        return(vme->SlaveUnmap(vsm));
    }
};

//------------------------------------------------------------------------------

class ItemSlaveMapDump : public MenuItem {
  public:
    ItemSlaveMapDump() { setName("Dump"); }
    int action() {

        return(vsm->Dump());
    }
};

//------------------------------------------------------------------------------

class ItemSlaveMapsDump : public MenuItem {
  public:
    ItemSlaveMapsDump() { setName("SlaveMapDump"); }
    int action() {

        return(vme->SlaveMapDump());
    }
};

//------------------------------------------------------------------------------

class ItemBlockTransfer : public MenuItem {
  public:
    ItemBlockTransfer() { setName("BlockTransfer"); }
    int action() {
        VMEBlockTransferList list;

        u_int vme_addr, tot_size, size;
        int ctrl,flag, i(0);

        tot_size = 0;
        while(1) {

            // read parameters from standard input
            vme_addr = (u_int)enterHex("vme address      (hex)",0,0xffffffff);
            if(vme_addr < 0x100) vme_addr = vme_addr << 24;
            size = (u_int)enterHex("size             (hex)",0,0x00ffffff);
            ctrl = enterInt("vme->pci/0; vme<-pci/1",0,1);

            // fill VME block transfer item
            list.list_of_items[i].vmebus_address        = vme_addr;
            list.list_of_items[i].system_iobus_address  = 0;
            list.list_of_items[i].size_requested        = size;
            list.list_of_items[i].control_word          = ctrl ? VME_DMA_D32W : VME_DMA_D32R;
            DEBUG_TEXT(DFDB_RCDVME,20,"vme=" << std::hex << std::setfill('0') << std::setw(8) << list.list_of_items[i].vmebus_address
                     << ", pci=" << std::setw(8) << list.list_of_items[i].system_iobus_address
                     << ", size=" << std::setw(8) << list.list_of_items[i].size_requested
                     << ", ctrl=" << std::setw(8) << list.list_of_items[i].control_word << std::dec);
            tot_size += size;
            i++;

            // iterate for chained block transfers
            flag = enterInt("continue",0,1);
            if(flag == 0) break;
        }
        list.number_of_items = i;

        if(tot_size > buf->Size()) {
            ERR_TEXT(DFDB_RCDVME,"total size bigger than event buffer");
            return(-1);
        }
        event_size = tot_size;

        // fill buffer address
        tot_size = 0;
        for(i=0; i!=list.number_of_items; i++) {
            list.list_of_items[i].system_iobus_address = buf->PhysicalAddress() + tot_size;
            tot_size += list.list_of_items[i].size_requested;
        }

        blk = vme->BlockTransfer(list);
        return((*blk)());
    }
};

//------------------------------------------------------------------------------

class ItemBlockTransferStart : public MenuItem {
  public:
    ItemBlockTransferStart() { setName("Start"); }
    int action() {

        u_int rtnv;

        if((rtnv = blk->Start()) != VME_SUCCESS) {
            ERR_TEXT(DFDB_RCDVME,"starting block transfer");
            vme->ErrorPrint(rtnv);
        }
        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ItemBlockTransferWait : public MenuItem {
  public:
    ItemBlockTransferWait() { setName("Wait"); }
    int action() {

        u_int rtnv, rtnw;

        int time = enterInt("time-out",-1,100000);

        if((rtnv = blk->Wait(time)) != VME_SUCCESS) {
            ERR_TEXT(DFDB_RCDVME,"waiting for block transfer");
            vme->ErrorPrint(rtnv);
            blk->Status(0,&rtnw);
            vme->ErrorPrint(rtnw);
        }
        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ItemBlockTransferDump : public MenuItem {
  public:
    ItemBlockTransferDump() { setName("Dump"); }
    int action() {

        return(blk->Dump());
    }
};

//------------------------------------------------------------------------------

class ItemBlockTransfersDump : public MenuItem {
  public:
    ItemBlockTransfersDump() { setName("BlockTransferDump"); }
    int action() {

        return(vme->BlockTransferDump());
    }
};
//------------------------------------------------------------------------------

class ItemEventBufferDump : public MenuItem {
  public:
    ItemEventBufferDump() { setName("EventBufferDump"); }
    int action() {

        int s = enterHex("number of words",0,0xffffffff);
        int i;

        u_long* data = (u_long*)buf->VirtualAddress();
        std::printf("ItemEventBufferDump: virt = %016lx ...\n",(u_long)data);

        for(i=0; i<s; i++) {
            if(i%8 == 0) std::printf("%04x",i);
            std::printf(" %16lx",data[i]);
            if(i%8==7 && i!=s) std::cout << std::endl;
        }
        std::cout << std::endl;

        return(0);
    }
};

//------------------------------------------------------------------------------

class ItemBlockTransferDelete : public MenuItem {
  public:
    ItemBlockTransferDelete() { setName("BlockTransferDelete"); }
    int action() {

        int rtnv = 0;

        if((rtnv = vme->BlockTransferDelete(blk)) != 0) {
            ERR_TEXT(DFDB_RCDVME,"deleting block transfer");
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ItemInterrupt : public MenuItem {
  public:
    ItemInterrupt() { setName("Interrupt"); }
    int action() {

        VMEInterruptList list;
        u_char v;
        u_int l,t;
        int flag, i(0);

        while(1) {
            v = (u_char)enterHex("vector",0,0xff);
            l = (u_int)enterInt("level  ",1,7);
            t = (u_int)enterInt("type   ",0,1);
            list.list_of_items[i].vector = v;
            list.list_of_items[i].level = l;
            list.list_of_items[i].type = t;
            i++;
            flag = enterInt("continue",0,1);
            if(flag == 0) break;
        }
        list.number_of_items = i;

        irq = vme->Interrupt(list);
        return((*irq)());
    }
};

//------------------------------------------------------------------------------

class ItemInterruptWait : public MenuItem {
  public:
    ItemInterruptWait() { setName("Wait"); }
    int action() {

        VMEInterruptInfo interrupt_info;
        u_int rtnv;

        int time = enterInt("time-out",-1,1000);

        if((rtnv = irq->Wait(time,interrupt_info)) != VME_SUCCESS) {
          ERR_TEXT(DFDB_RCDVME,"");
          vme->ErrorPrint(rtnv);
        }
        else {
          DEBUG_TEXT(DFDB_RCDVME,20,"level = " << std::dec << interrupt_info.level
                     << ", vector = " << std::hex << std::setfill('0') << std::setw(2) << interrupt_info.vector
                     << ", mult = " << std::setw(8) << interrupt_info.multiple << std::dec);
        }
        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class ItemInterruptRegisterSignal : public MenuItem {
  public:
    ItemInterruptRegisterSignal() { setName("RegisterSignal"); }
    int action() {

        struct sigaction sa;

        int s = enterInt("signal",0,256);

        sa.sa_handler = (void (*)(int))interrupt_handler;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        if(sigaction(s,&sa,(struct sigaction *)0)) {
            ERR_TEXT(DFDB_RCDVME,"installing interrupt handler");
            return(-1);
        }

        return(irq->RegisterSignal(s));
    }
};

//------------------------------------------------------------------------------

class ItemInterruptInfoGet : public MenuItem {
  public:
    ItemInterruptInfoGet() { setName("InfoGet"); }
    int action() {

        VMEInterruptInfo interrupt_info;
        u_int rtnv;

        if((rtnv = irq->InfoGet(interrupt_info)) != VME_SUCCESS) {
          DEBUG_TEXT(DFDB_RCDVME,20,"level = " << std::dec << interrupt_info.level
                     << ", vector = " << std::hex << std::setfill('0') << std::setw(2) << interrupt_info.vector
                     << ", mult = " << interrupt_info.multiple << std::dec);
        }
        else {
          ERR_TEXT(DFDB_RCDVME,"");
            vme->ErrorPrint(rtnv);
        }
         return(rtnv);

    }
};

//------------------------------------------------------------------------------

class ItemInterruptReenable : public MenuItem {
  public:
    ItemInterruptReenable() { setName("Reenable"); }
    int action() {

        return(irq->Reenable());
    }
};

//------------------------------------------------------------------------------

class ItemInterruptGenerate : public MenuItem {
  public:
    ItemInterruptGenerate() { setName("InterruptGenerate"); }
    int action() {

        int l = enterInt("level ",1,7);
        u_char v = (u_char)enterHex("vector",0,0xff);
        return(vme->InterruptGenerate(l,v));
    }
};

//------------------------------------------------------------------------------

class ItemInterruptDump : public MenuItem {
  public:
    ItemInterruptDump() { setName("Dump"); }
    int action() {

        return(irq->Dump());
    }
};

//------------------------------------------------------------------------------

class ItemInterruptsDump : public MenuItem {
  public:
    ItemInterruptsDump() { setName("InterruptDump"); }
    int action() {

        return(vme->InterruptDump());
    }
};

//------------------------------------------------------------------------------

class ItemInterruptDelete : public MenuItem {
  public:
    ItemInterruptDelete() { setName("InterruptDelete"); }
    int action() {

        vme->InterruptDelete(irq);
        return(0);
    }
};

//------------------------------------------------------------------------------

int main() {

    // Define debug settings
    DF::GlobalDebugSettings::setup(20,DFDB_RCDVME);

    Menu menu("VME");
    Menu menuBUS("BusError");
    Menu menuMST("MasterMap");
    Menu menuSLV("SlaveMap");
    Menu menuBLK("BlockTransfer");
    Menu menuINT("Interrupt");

    // master mapping
    menuBUS.add(new ItemBusErrorRegisterSignal);
    menuBUS.add(new ItemBusErrorInfoGet);

    // master mapping
    menuMST.init(new ItemMasterMap);
    menuMST.add(new ItemRead);
    menuMST.add(new ItemWrite);
    menuMST.add(new ItemVirtualAddress);
    menuMST.add(new ItemMasterMapDump);
    menuMST.add(new ItemMasterMapsDump);
    menuMST.exit(new ItemMasterUnmap);

    // slave mapping
    menuSLV.init(new ItemSlaveMap);
    menuSLV.add(new ItemVmebusAddress);
    menuSLV.add(new ItemSlaveMapDump);
    menuSLV.add(new ItemSlaveMapsDump);
    menuSLV.exit(new ItemSlaveUnmap);

    // block transfer
    menuBLK.init(new ItemBlockTransfer);
    menuBLK.add(new ItemBlockTransferStart);
    menuBLK.add(new ItemBlockTransferWait);
    menuBLK.add(new ItemBlockTransferDump);
    menuBLK.add(new ItemEventBufferDump);
    menuBLK.add(new ItemBlockTransfersDump);
    menuBLK.exit(new ItemBlockTransferDelete);

    // interrupt
    menuINT.init(new ItemInterrupt);
    menuINT.add(new ItemInterruptWait);
    menuINT.add(new ItemInterruptRegisterSignal);
    menuINT.add(new ItemInterruptInfoGet);
    menuINT.add(new ItemInterruptReenable);
    menuINT.add(new ItemInterruptDump);
    menuINT.add(new ItemInterruptsDump);
    menuINT.exit(new ItemInterruptDelete);

    // main menu
    menu.init(new ItemOpen);
    menu.add(&menuBUS);
    menu.add(&menuMST);
    menu.add(&menuSLV);
    menu.add(&menuBLK);
    menu.add(&menuINT);
    menu.add(new ItemInterruptGenerate);
    menu.exit(new ItemClose);

    // execute main menu
    menu.execute();
}
