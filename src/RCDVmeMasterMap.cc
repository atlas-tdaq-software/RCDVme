//******************************************************************************
// file: RCDVmeMasterMap.cc
// desc: VMEbus C++ wrapper library, VME master mapping class
// auth: 17/09/01 R. Spiwoks
// modf: 23/10/01 R. Spiwoks, VMEbus API draft
// modf: 02/12/02 R. Spiwoks, DataFlow repository
// modf: 19/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
//******************************************************************************

// $Id$

#include <iostream>
#include <iomanip>
#include <cstdio>

#include "DFDebug/DFDebug.h"
#include "RCDVme/RCDVmeMasterMap.h"

using namespace RCD;

//------------------------------------------------------------------------------

u_int VMEMasterMap::ReadSafe(u_int address_offset, u_int* data) {

    DEBUG_TEXT(DFDB_RCDVME,20,"virt = " << std::hex << std::setfill('0') << std::setw(8)
               << (u_int)(my_virtual_address + address_offset)
               << ", &data = " << std::setw(8) << data << std::dec);

    return(VME_ReadSafeUInt(my_identifier,address_offset,data));
}

//------------------------------------------------------------------------------

u_int VMEMasterMap::WriteSafe(u_int address_offset, u_int data) {

    DEBUG_TEXT(DFDB_RCDVME,20,"virt = " << std::hex << std::setfill('0') << std::setw(8)
               << (u_int)(my_virtual_address + address_offset)
               << ", data = " << std::setw(8) << data << std::dec);

    return(VME_WriteSafeUInt(my_identifier, address_offset,data));
}

//------------------------------------------------------------------------------

u_int VMEMasterMap::ReadSafe(u_int address_offset, u_short* data) {

    DEBUG_TEXT(DFDB_RCDVME,20,"virt = " << std::hex << std::setfill('0') << std::setw(8)
               << (u_int)(my_virtual_address + address_offset)
               << ", &data = " << std::setw(8) << data << std::dec);

    return(VME_ReadSafeUShort(my_identifier, address_offset,data));
}

//------------------------------------------------------------------------------

u_int VMEMasterMap::WriteSafe(u_int address_offset, u_short data) {

    DEBUG_TEXT(DFDB_RCDVME,20,"virt = " << std::hex << std::setfill('0') << std::setw(8)
               << (u_int)(my_virtual_address + address_offset)
               << ", data = " << std::setw(4) << data << std::dec);

    return(VME_WriteSafeUShort(my_identifier, address_offset,data));
}

//------------------------------------------------------------------------------

u_int VMEMasterMap::ReadSafe(u_int address_offset, u_char* data) {

    DEBUG_TEXT(DFDB_RCDVME,20,"virt = " << std::hex << std::setfill('0') << std::setw(8)
               << (u_int)(my_virtual_address + address_offset)
               << ", &data = " << std::setw(8) << data << std::dec);

    return(VME_ReadSafeUChar(my_identifier, address_offset,data));
}

//------------------------------------------------------------------------------

u_int VMEMasterMap::WriteSafe(u_int address_offset, u_char data) {

    DEBUG_TEXT(DFDB_RCDVME,20,"virt = " << std::hex << std::setfill('0') << std::setw(8)
               << (u_int)(my_virtual_address + address_offset)
               << ", data = " << std::setw(2) << (u_short)data << std::dec);

    return(VME_WriteSafeUChar(my_identifier, address_offset,data));
}

//------------------------------------------------------------------------------

u_int VMEMasterMap::Dump() const {

    std::printf("RCD::VMEMasterMap::Dump: id = %d, phys = %08x, virt = %08lx\n",my_identifier,my_master_map.vmebus_address,my_virtual_address);

    return(my_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

VMEMasterMap::VMEMasterMap(u_int vmebus_address, u_int window_size, u_int address_modifier, u_int options) {

    // initialize private master map structure
    my_master_map.vmebus_address        = vmebus_address;
    my_master_map.window_size           = window_size;
    my_master_map.address_modifier      = address_modifier;
    my_master_map.options               = options;

    DEBUG_TEXT(DFDB_RCDVME,20,"addr = " << std::hex << std::setfill('0') << std::setw(8) << my_master_map.vmebus_address
               << ", size = " << std::setw(8) << my_master_map.window_size
               << ", am = " << std::setw(2) << my_master_map.address_modifier
               << ", opt = " << std::setw(2) << my_master_map.options << std::dec);

    // create VME master mapping
    if((my_status = VME_MasterMap(&my_master_map,&my_identifier)) != VME_SUCCESS) {
        VME_ErrorPrint(my_status);
        ERR_TEXT(DFDB_RCDVME,"mapping master mapping, addr = " << std::hex << std::setfill('0') << std::setw(8) << my_master_map.vmebus_address << std::dec);
        return;
    }

    // get virtual address
    if((my_status = VME_MasterMapVirtualLongAddress(my_identifier,&my_virtual_address)) != VME_SUCCESS) {
        VME_ErrorPrint(my_status);
        ERR_TEXT(DFDB_RCDVME,"reading virtual address for master mapping, id = " << std::dec << my_identifier);
        return;
    }

    DEBUG_TEXT(DFDB_RCDVME,20,"id = " << std::dec << my_identifier
             << ", phys = " << std::hex << std::setfill('0') << std::setw(8) << my_master_map.vmebus_address
             << ", virt = " << std::setw(8) << my_virtual_address << std::dec);
}

//------------------------------------------------------------------------------

VMEMasterMap::~VMEMasterMap() {

    if((my_status = VME_MasterUnmap(my_identifier)) != VME_SUCCESS) {
        ERR_TEXT(DFDB_RCDVME,"unmapping master mapping id = " << std::dec << my_identifier
                 << ", virt = " << std::hex << std::setfill('0') << std::setw(8) << my_virtual_address << std::dec);
        return;
    }
    DEBUG_TEXT(DFDB_RCDVME,20,"id = " << std::dec << my_identifier
             << ", phys = " << std::hex << std::setfill('0') << std::setw(8) << my_master_map.vmebus_address
             << ", virt = " << std::setw(8) << my_virtual_address << std::dec);
}
