//******************************************************************************
// file: RCDVmeInterrupt.cc
// desc: VMEbus C++ wrapepr library, VMEbus interrupt class
// auth: 23/10/01 R. Spiwoks
// modf: 02/12/02 R. Spiwoks, DataFlow repository
// modf: 19/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
//******************************************************************************

// $Id$

#include <iostream>
#include <cstdio>

#include "DFDebug/DFDebug.h"
#include "RCDVme/RCDVmeInterrupt.h"

using namespace RCD;

//------------------------------------------------------------------------------

u_int VMEInterrupt::Wait(int time_out, VMEInterruptInfo& interrupt_info) {

    return(VME_InterruptWait(my_identifier,time_out,&interrupt_info));
}

//------------------------------------------------------------------------------

u_int VMEInterrupt::RegisterSignal(int signal_number) {

    return(VME_InterruptRegisterSignal(my_identifier,signal_number));
}

//------------------------------------------------------------------------------

u_int VMEInterrupt::InfoGet(VMEInterruptInfo& interrupt_info) {

    return(VME_InterruptInfoGet(my_identifier,&interrupt_info));
}

//------------------------------------------------------------------------------

u_int VMEInterrupt::Reenable() {

    return(VME_InterruptReenable(my_identifier));
}

//------------------------------------------------------------------------------

u_int VMEInterrupt::Dump() const {

    int i;

    std::printf("RCD::VMEInterrupt::Dump: %d item(s) ...\n",my_interrupt_list.number_of_items);
    for(i=0; i<my_interrupt_list.number_of_items; i++) {
        std::printf("    level = %d, vector = %02x, type = %01x\n",my_interrupt_list.list_of_items[i].level,my_interrupt_list.list_of_items[i].vector,my_interrupt_list.list_of_items[i].type);
    }

    return(my_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

VMEInterrupt::VMEInterrupt(const VMEInterruptList& interrupt_list) :

    // copy interrupt list: not nice but needed for encapsulation
    my_interrupt_list(interrupt_list) {

    // link interrupt list
    if((my_status = VME_InterruptLink(&my_interrupt_list, &my_identifier)) != VME_SUCCESS) {
        VME_ErrorPrint(my_status);
        ERR_TEXT(DFDB_RCDVME,"linking interrupt list");
        return;
    }
}

//------------------------------------------------------------------------------

VMEInterrupt::~VMEInterrupt() {

    // unlink interrupt list
    if((my_status = VME_InterruptUnlink(my_identifier)) != VME_SUCCESS) {
        VME_ErrorPrint(my_status);
        ERR_TEXT(DFDB_RCDVME,"unlinking interrupt list");
        return;
    }
}
