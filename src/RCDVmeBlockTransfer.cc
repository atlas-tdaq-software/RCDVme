//******************************************************************************
// file: RCDVmeBlockTransfer.cc
// desc: VMEbus C++ wrapper library, VME block transfer
// auth: 19/09/01 R. Spiwoks
// modf: 23/10/01 R. Spiwoks, VMEbus API draft
// modf: 02/12/02 R. Spiwoks, DataFlow repository
// modf: 19/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
///******************************************************************************

// $Id$

#include <iostream>
#include <iomanip>
#include <cstdio>

#include "DFDebug/DFDebug.h"
#include "RCDVme/RCDVmeBlockTransfer.h"

using namespace RCD;

//------------------------------------------------------------------------------

u_int VMEBlockTransfer::Start() {

    return(VME_BlockTransferStart(my_identifier));
}

//------------------------------------------------------------------------------

u_int VMEBlockTransfer::Wait(int time_out) {

    return(VME_BlockTransferWait(my_identifier, time_out, &my_block_transfer_list));
}

//------------------------------------------------------------------------------

u_int VMEBlockTransfer::Status(int position_of_block, u_int* status) const {

    *status =
        my_block_transfer_list.list_of_items[position_of_block].status_word;

    return(my_status);
}

//------------------------------------------------------------------------------

u_int VMEBlockTransfer::Remaining(int position_of_block, u_int* remaining) const {

    *remaining =
        my_block_transfer_list.list_of_items[position_of_block].size_remaining;

    return(my_status);
}

//------------------------------------------------------------------------------

u_int VMEBlockTransfer::Dump() const {

    int i, num(my_block_transfer_list.number_of_items);

    std::cout << "=========================================================================" << std::endl;
    std::printf("RCD::VMEBlockTransfer::Dump: %d item%s:\n",num,num>1?"s":"");
    std::cout << "-------------------------------------------------------------------------" << std::endl;
    for(i=0; i<num; i++) {
        std::printf("item %2d: vme = %08x, pci = %08lx, size = %08x, ctrl = %08x\n",i,my_block_transfer_list.list_of_items[i].vmebus_address,my_block_transfer_list.list_of_items[i].system_iobus_address,my_block_transfer_list.list_of_items[i].size_requested,my_block_transfer_list.list_of_items[i].control_word);
        std::printf("                                         rest = %08x, stat = %08x\n",my_block_transfer_list.list_of_items[i].size_remaining,my_block_transfer_list.list_of_items[i].status_word);
        if(i<(num-1)) std::cout << "-------------------------------------------------------------------------" << std::endl;
    }
    std::cout << "=========================================================================" << std::endl;

    return(my_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

                                                                                      // copy block transfer list: not nice, but needed for encapsulation
VMEBlockTransfer::VMEBlockTransfer(const VMEBlockTransferList& block_transfer_list) : my_block_transfer_list(block_transfer_list) {

#ifdef DEBUG
    Dump();
#endif

    // initialize block transfer
    if((my_status = VME_BlockTransferInit(&my_block_transfer_list, &my_identifier)) != VME_SUCCESS) {
        VME_ErrorPrint(my_status);
        ERR_TEXT(DFDB_RCDVME,"initializing block transfer");
        return;
    }
}

//------------------------------------------------------------------------------

VMEBlockTransfer::~VMEBlockTransfer() {

    // end block transfer
    if((my_status = VME_BlockTransferEnd(my_identifier)) != VME_SUCCESS) {
        ERR_TEXT(DFDB_RCDVME,"ending block transfer");
        return;
    }
}
