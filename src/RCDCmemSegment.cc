//******************************************************************************
// file: RCDCmemMSegment.cc
// desc: C++ wrapper for RCD CMEM driver/library
// auth: 06/06/02 R. Spiwoks
// modf: 02/12/02 R. Spiwoks, DataFlow repository
// modf: 19/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
// modf: 13/12/05 R. Spiwoks: bug fix for user count + error messages
// modf: 04/05/10 E. Pasqualucci: compiled on 64 bits machine - DOES NOT MEAN that it works!!!!
//******************************************************************************

// $Id$

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <stdint.h>
#include <inttypes.h>

#include "DFDebug/DFDebug.h"
#include "RCDVme/RCDCmemSegment.h"
#include "RCDUtilities/RCDUtilities.h"

using namespace RCD;

//------------------------------------------------------------------------------

int CMEMSegment::my_user = 0;

//------------------------------------------------------------------------------

CMEMSegment::CMEMSegment(std::string name, u_int size, bool bpa) : my_name(name), my_size(size), my_bpa(bpa) {

    // open CMEM driver/library if necessary
    if(my_user++ == 0) {
        if((my_status = CMEM_Open()) != CMEM_RCC_SUCCESS) {
            CERR("opening RCD CMEM driver/library:","");
            std::cerr << ">> "; rcc_error_print(stderr,my_status);
            return;
        }
    }
    DEBUG_TEXT(DFDB_RCDVME,20,"user = " << std::dec << my_user);

    // allocate CMEM segment
    if(!bpa) {
        if((my_status = CMEM_SegmentAllocate(my_size,const_cast<char*>(my_name.c_str()),&my_identifier)) != CMEM_RCC_SUCCESS) {
            CERR("allocating segment \"%s\" with size 0x%08x:",my_name.c_str(),my_size);
            std::cerr << ">> "; rcc_error_print(stderr,my_status);
            return;
        }
    }
    else {
        if((my_status = CMEM_BPASegmentAllocate(my_size,const_cast<char*>(my_name.c_str()),&my_identifier)) != CMEM_RCC_SUCCESS) {
            CERR("allocating segment \"%s\" (BPA) with size 0x%08x:",my_name.c_str(),my_size);
            std::cerr << ">> "; rcc_error_print(stderr,my_status);
            return;
        }
    }

    // get virtual address
    if((my_status = CMEM_SegmentVirtualAddress(my_identifier,&my_virtual_address)) != CMEM_RCC_SUCCESS) {
        CERR("getting virtual address for segment \"%s\", id = %d:",my_name.c_str(),my_identifier);
        std::cerr << ">> "; rcc_error_print(stderr,my_status);
        return;
    }

    // get phsyical address
    if((my_status = CMEM_SegmentPhysicalAddress(my_identifier,&my_physical_address)) != CMEM_RCC_SUCCESS) {
        CERR("getting physical address for segment \"%s\", id = %d:",my_name.c_str(),my_identifier);
        std::cerr << ">> "; rcc_error_print(stderr,my_status);
        return;
    }

    // print info
    DEBUG_TEXT(DFDB_RCDVME,20,"id = " << std::dec << my_identifier
             << ", name = " << my_name.c_str()
             << ", size = " << std::hex << std::setfill('0') << std::setw(8) << my_size
             << (my_bpa?", type = BPA":"")
             << ", virt = " << std::setw(8) << my_virtual_address
             << ", phys = " << std::setw(8) << my_physical_address << std::dec);
    CDBG("id = %d, name = \"%s\", size = 0x%08x, type = %s, virt = 0x%08x, phys = 0x%08x",my_identifier,my_name.c_str(),my_size,(my_bpa?"BPA":"KERNEL"),my_virtual_address,my_physical_address);
}

//------------------------------------------------------------------------------

CMEMSegment::~CMEMSegment() {

    // free segment
    if(!my_bpa) {
        if((my_status = CMEM_SegmentFree(my_identifier)) != CMEM_RCC_SUCCESS) {
            CERR("freeing segment \"%s\", id = %d:",my_name.c_str(),my_identifier);
            std::cerr << ">> "; rcc_error_print(stderr,my_status);
            return;
        }
    }
    else {
        if((my_status = CMEM_BPASegmentFree(my_identifier)) != CMEM_RCC_SUCCESS) {
            CERR("freeing segment \"%s\" (BPA), id = %d:",my_name.c_str(),my_identifier);
            std::cerr << ">> "; rcc_error_print(stderr,my_status);
            return;
        }
    }

    // close RCD CMEM driver/library if necessary
    DEBUG_TEXT(DFDB_RCDVME,20,"user = " << std::dec << my_user);
    if(--my_user == 0) {
        if((my_status = CMEM_Close()) != CMEM_RCC_SUCCESS) {
            CERR("closing RCD CMEM driver/library:","");
            std::cerr << ">> "; rcc_error_print(stderr,my_status);
            return;
        }
    }
    CDBG("id = %d, name = \"%s\", size = 0x%08x, type = %s, virt = 0x%08x, phys = 0x%08x",my_identifier,my_name.c_str(),my_size,(my_bpa?"BPA":"KERNEL"),my_virtual_address,my_physical_address);
}

//------------------------------------------------------------------------------

u_int CMEMSegment::Dump() {

    return(CMEM_Dump());
}

//------------------------------------------------------------------------------

int CMEMSegment::dump(int size) const {

    // dump general information
    std::printf("CMEM segment %s, virt = %016lx, phys = %16lx, size = %d\n",(my_bpa?"(BPA)":""),my_virtual_address,this->PhysicalAddress(),this->Size());
    if(size > (int)(this->Size()/sizeof(u_int))) {
        CERR("illegal size \"%d\"",size);
        return(-1);
    }

    // dump data
    long unsigned int* data = (long unsigned int*)my_virtual_address;
    int i;
    for(i=0; i<size; i++) {
        if(i%16 == 0) std::printf("%06x",i);
        std::printf(" %08lx",data[i]);
        if(i%16 == 15) std::cout << std::endl;
    }
    if(i%16 != 0) std::cout << std::endl;

    return(0);
}

//------------------------------------------------------------------------------

int CMEMSegment::fill(int size, u_int word, u_int inc) {

    u_int* data = (u_int*)this->VirtualAddress();
    u_int value = word;
    int i;

    // check size of CMEM segment
    if(this->Size() < (size * sizeof(u_int))) {
        CERR("Segment too small, cmem = 0x%08x, size = 0x%08x",this->Size(),size);
        return(-1);
    }

    // fill data
    for(i = 0; i < size; i++) {
        data[i] = value;
        value += inc;
    }

    return(0);
}

//------------------------------------------------------------------------------

int CMEMSegment::writeToFile(const std::string& fn, int off) {

    // check offset
    if(off * sizeof(u_int) > this->Size()) {
        CERR("offset (%d) greater than segment size (%d)",off,this->Size()/sizeof(u_int));
        return(-1);
    }

    // open out file
    std::ofstream outf(fn.c_str()); if(! outf) {
        CERR("cannot open output file \"%s\"",fn.c_str());
        return(-1);
    }

    // read data from segment memory and write to file
    std::ostringstream  buf;
    u_int* data = (u_int*)this->VirtualAddress();
    int i;
    for(i=0; i<off; i++) {
        buf.str(""); buf << "0x" << std::hex << std::setw(8) << std::setfill('0') << data[i] << std::dec << std::setfill(' ');
        if(! (outf << buf.str() << std::endl)) {
            CERR("writing word %d to output file \"%s\"",i,fn.c_str());
            return(-1);
        }
    }
    COUT("CMEM \"%s\": wrote %d data words to output file \"%s\"",my_name.c_str(),i,fn.c_str());

    return(0);
}

//------------------------------------------------------------------------------

int CMEMSegment::readFromFile(const std::string& fn, int off) {

    // check offset
    if(off > (int)(this->Size()/sizeof(u_int))) {
        CERR("offset (%d) greater than segment size (%d)",off,this->Size()/sizeof(u_int));
        return(-1);
    }

    // open input file
    std::ifstream inf(fn.c_str()); if(! inf) {
        CERR("cannot open input file \"%s\"",fn.c_str());
        return(-1);
    }

    // read data from file and write to segment memory
    std::string buf;
    u_int* data = (u_int*)this->VirtualAddress();
    int i;
    for(i=0; i<off; i++) {
        if(! (inf >> buf)) {
            CERR("reading word %d from input file \"%s\"",i,fn.c_str());
            return(-1);
        }
        std::sscanf(buf.c_str(),"%x",&data[i]);
    }
    COUT("CMEM \"%s\": read %d data words from input file \"%s\"",my_name.c_str(),i,fn.c_str());

    return(0);
}
