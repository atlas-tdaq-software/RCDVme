//******************************************************************************
// file: RCDVmeSlaveMap.cc
// desc: VMEbus C++ wrapepr library, VME slave mapping class
// auth: 19/09/01 R. Spiwoks
// modf: 23/10/01 R. Spiwoks, VMEbus API draft
// modf: 02/12/02 R. Spiwoks, Dataflow repository
// modf: 19/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
//******************************************************************************

// $Id$

#include <iomanip>

#include "DFDebug/DFDebug.h"
#include "RCDVme/RCDVmeSlaveMap.h"

using namespace RCD;

//------------------------------------------------------------------------------

u_int VMESlaveMap::Dump() const {

    printf("RCD::VmeSlaveMap::Dump: id = %d, pci = %08llx, vme = %08x\n",my_identifier,my_slave_map.system_iobus_address,my_vmebus_address);

    return(my_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

VMESlaveMap::VMESlaveMap(u_int system_iobus_address, u_int window_size, u_int address_width, u_int options) {

    // initialize private slave map structure
    my_slave_map.system_iobus_address   = system_iobus_address;
    my_slave_map.window_size            = window_size;
    my_slave_map.address_width          = address_width;
    my_slave_map.options                = options;

    DEBUG_TEXT(DFDB_RCDVME,20,"addr = " << std::hex << std::setfill('0') << std::setw(8) << my_slave_map.system_iobus_address
               << ", size = " << std::setw(8) << my_slave_map.window_size
               << ", aw = " << std::setw(2) << my_slave_map.address_width
               << ", opt = " << std::setw(2) << my_slave_map.options << std::dec);

    // create VME slave mapping
    if((my_status = VME_SlaveMap(&my_slave_map, &my_identifier)) != VME_SUCCESS) {
        VME_ErrorPrint(my_status);
        ERR_TEXT(DFDB_RCDVME,"mapping slave mapping, addr = " << std::hex << std::setfill('0') << std::setw(8) << my_slave_map.system_iobus_address << std::dec);
        return;
    }

    // get VMEbus address
    if((my_status = VME_SlaveMapVmebusAddress(my_identifier,&my_vmebus_address)) != VME_SUCCESS) {
        VME_ErrorPrint(my_status);
        ERR_TEXT(DFDB_RCDVME,"reading VMEbus address for slave mapping, id = " << std::dec << my_identifier);
        return;
    }

    DEBUG_TEXT(DFDB_RCDVME,20,"id = " << std::dec << my_identifier
             << ", pci = " << std::hex << std::setfill('0') << std::setw(8) << my_slave_map.system_iobus_address
             << ", vme = " << std::setw(8) << my_vmebus_address << std::dec);
}

//------------------------------------------------------------------------------

VMESlaveMap::~VMESlaveMap() {

    if((my_status = VME_SlaveUnmap(my_identifier)) != VME_SUCCESS) {
      ERR_TEXT(DFDB_RCDVME,"unmapping slave mapping, id = " << std::dec << my_identifier
               << ", vme = " << std::hex << std::setfill('0') << std::setw(8) << my_vmebus_address << std::dec);
        return;
    }
    DEBUG_TEXT(DFDB_RCDVME,20,"id = " << std::dec << my_identifier
             << ", pci = " << std::hex << std::setfill('0') << std::setw(8) << my_slave_map.system_iobus_address
             << ", vme = " << std::setw(8) << my_vmebus_address << std::dec);
}
